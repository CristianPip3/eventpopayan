package com.px.linuxp.eventpopayan.Actividades.Perfil.Organizador;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.px.linuxp.eventpopayan.Adapters.CommentsAdapter;
import com.px.linuxp.eventpopayan.Modelo.Comentario;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;

public class ComentariosActivity extends AppCompatActivity {

    private static final String TAG = ComentariosActivity.class.getSimpleName();
    public static ArrayList<Comentario> misEventos =new ArrayList<Comentario>();
    private CommentsAdapter adapter;
    private RecyclerView lista;
    private RecyclerView.LayoutManager lManager;
    String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lista = (RecyclerView) findViewById(R.id.comt_recicler_comments);
        lista.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        lista.setLayoutManager(lManager);
        procesarRespuesta();



    }

    public void procesarRespuesta() {

        Comentario coment1 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment2 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment3 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment4 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment5 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment6 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");
        Comentario coment7 = new Comentario("1","2","1","Cristian Felipe","03-11-2017","Muy Buen vieja","5");



        List<Comentario> comentarios = new ArrayList<>();
        comentarios.add(coment1);
        comentarios.add(coment2);
        comentarios.add(coment3);
        comentarios.add(coment4);
        comentarios.add(coment5);
        comentarios.add(coment6);
        comentarios.add(coment7);


        adapter = new CommentsAdapter(comentarios,this);
        // Setear adaptador a la lista
        lista.setAdapter(adapter);
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

}
