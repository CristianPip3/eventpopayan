package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PublicFragment;
import com.px.linuxp.eventpopayan.Actividades.GUI.InicioActivity;
import com.px.linuxp.eventpopayan.Actividades.GUI.Registro;
import com.px.linuxp.eventpopayan.Herramientas.Entites.AccessToken;
import com.px.linuxp.eventpopayan.Herramientas.Entites.ApiError;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Persistencia.Utils;
import com.px.linuxp.eventpopayan.R;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 26/01/18.
 */

public class RegisterAssitentFragment extends PublicFragment {

    private static final String TAG = "RegisterAssitent";


    private Button mbButtonRegistroAssistente;
    private TextInputLayout mTilNombre;
    private TextInputLayout mTilEmail;
    private TextInputLayout mTilPasswor;
    private SharedPreferences prefs;
    private View rootView;
    LoginActivity.ConnectReceiver connectReceiver;
    IntentFilter intentFilter;

    ApiService service;
    Call<AccessToken> call;
    AwesomeValidation validator;
    TokenManager tokenManager;

    public RegisterAssitentFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragemnt_register_assitent
                , container, false);
        mTilNombre = (TextInputLayout) rootView.findViewById(R.id.til_nombre);
        mTilEmail = (TextInputLayout) rootView.findViewById(R.id.til_email);
        mTilPasswor = (TextInputLayout) rootView.findViewById(R.id.til_password);
        mbButtonRegistroAssistente = (Button) rootView.findViewById(R.id.email_register_button);
        service = RetrofitBuilder.createService(ApiService.class);
        prefs = this.getActivity().getSharedPreferences("prefs",getContext().MODE_PRIVATE);
        tokenManager = TokenManager.getInstance(prefs);
        intentFilter = new IntentFilter();
        intentFilter.addAction(ServiceConect.terminoOk);
        intentFilter.addAction(ServiceConect.terminoError);
        connectReceiver = new RegisterAssitentFragment().connectReceiver;
        getActivity().registerReceiver(connectReceiver, intentFilter);

        validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        setupRules();
        mbButtonRegistroAssistente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();

            }
        });
        if(tokenManager.getToken().getAccessToken() != null){
            if(tokenManager.isLoadedData()){
                startActivity(new Intent(getActivity(), OrganizadorMainActivity.class));

            }else {
                startActivity(new Intent(getActivity(), UserMainActivity.class));
            }


        }
        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(call != null){
            call.cancel();
            call = null;
        }
    }
    void register(){
        String name = mTilNombre.getEditText().getText().toString();
        String email = mTilEmail.getEditText().getText().toString();
        String password = mTilPasswor.getEditText().getText().toString();
        mTilNombre.setError(null);
        mTilEmail.setError(null);
        mTilPasswor.setError(null);
        validator.clear();

        if(validator.validate()) {
            call = service.register(name, email, password);
            call.enqueue(new Callback<AccessToken>() {
                @Override
                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                    Log.w(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {

                        tokenManager.saveToken(response.body());
                        Snackbar.make(rootView, "Registro Exitoso", Snackbar.LENGTH_LONG).show();
                        hacerRegistro();

                    } else {
                        handleErrors(response.errorBody());

                    }
                }

                @Override
                public void onFailure(Call<AccessToken> call, Throwable t) {
                    Snackbar.make(rootView, "ErrorConectiom", Snackbar.LENGTH_LONG).show();
                    Log.w(TAG, "onFailure: " + t.getMessage());

                }
            });
        }
    }
    public  void hacerRegistro(){
        Intent intent = new Intent(getActivity(),ServiceConect.class);
        getActivity().startService(intent);

    }
    private void handleErrors(ResponseBody response){

        ApiError apiError = Utils.converErrors(response);

        for(Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()){
            if(error.getKey().equals("name")){
                mTilNombre.setError(error.getValue().get(0));
            }
            if(error.getKey().equals("email")){
                mTilEmail.setError(error.getValue().get(0));
            }
            if(error.getKey().equals("password")){
                mTilPasswor.setError(error.getValue().get(0));
            }
        }

    }
    private void viewDialog(){


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.seleccionar_rol, null);
        mBuilder.setView(mView);
        Button buttonCrearCuenta = mView.findViewById(R.id.rol_btn_reg_org);
        Button buttonUser = mView.findViewById(R.id.rol_btn_reg_asis);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        buttonCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrganizadorMainActivity.class);
                startActivity(intent);
                dialog.dismiss();

            }
        });
        buttonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UserMainActivity.class);
                startActivity(intent);
                dialog.dismiss();

            }
        });



    }
    public void setupRules(){

        validator.addValidation(getActivity(), R.id.til_nombre, RegexTemplate.NOT_EMPTY, R.string.err_name);
        validator.addValidation(getActivity(), R.id.til_email, Patterns.EMAIL_ADDRESS, R.string.err_email);
        validator.addValidation(getActivity(), R.id.til_password, "[a-zA-Z0-9]{6,}", R.string.err_password);
    }
    public class ConnectReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch(intent.getAction()){
                case ServiceConect.terminoOk:
                    viewDialog();
                    break;
                case ServiceConect.terminoError:

                    break;


            }
            getActivity().unregisterReceiver(connectReceiver);
        }
    }

}
