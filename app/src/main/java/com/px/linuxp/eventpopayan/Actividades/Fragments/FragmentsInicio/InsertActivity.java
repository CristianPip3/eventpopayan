package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.app.Dialog;

import com.px.linuxp.eventpopayan.Actividades.Fragments.InsertEventFragment;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Modelo.ResponseInsert;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Calendar;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by linuxp on 11/11/17.
 */

public class InsertActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = InsertActivity.class.getSimpleName();

    ApiService service;
    Call<ResponseInsert> call;
    TokenManager tokenManager;
    private SharedPreferences prefs;
    private View mInsertFormView;
    private TextInputLayout txtIname;
    private MaterialSpinner txtIcategoria;
    private TextInputLayout txtIdescripcion;
    private TextInputLayout txtIpunto_encuentro;
    private TextInputLayout txtIrespondable;
    //private TextInputLayout txtIfecha_inicio;
    private TextInputLayout txtIimagen_evento;
    private TextInputLayout txtIprecio;
    //private TextInputLayout txtIfecha_fin;
    private Button btnRegistar;
    int dia,mes,anio,hora,minutos;
    PersistenUserSesion persistenUserSesion;

    private TextView fechaFinLbl, fechaInicioLbl,horaLbl;
    private TextView fechaFinIt,fechaInicioIt,horaE;
    String categoria;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.publicar_evento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarcrud);
        setSupportActionBar(toolbar);
        mInsertFormView = findViewById(R.id.mforminser);
        txtIname = findViewById(R.id.txt_titulo_input_it);
        persistenUserSesion = new PersistenUserSesion(this);


        txtIcategoria = findViewById(R.id.pubEve_spn_categoria);
        txtIdescripcion = findViewById(R.id.txt_descripcion_input_it);
        horaE =  findViewById(R.id.pubEve_txt_horaI);

        txtIprecio = findViewById(R.id.precio_texto);
        txtIpunto_encuentro = findViewById(R.id.pub_evnt_lgr_e);
        txtIrespondable = findViewById(R.id.pubEvent_ItTexRes);

        //txtIfecha_inicio = fechaFinIt.getText().toString();
        txtIimagen_evento = findViewById(R.id.txt_titulo_input_it);

        //txtIfecha_fin = findViewById(R.id.txt_titulo_input_it);
        btnRegistar = findViewById(R.id.pubEve_btn_publicar);
        final Calendar c= Calendar.getInstance();
        dia=c.get(Calendar.DAY_OF_MONTH);
        mes=c.get(Calendar.MONTH);
        anio=c.get(Calendar.YEAR);


        horaLbl =  findViewById(R.id.pubEve_lbl_horaI);

        horaLbl.setOnClickListener(this);


        String[] SPINNERCATEGORIAS = {"Gastronomia", "Recreativo", "Religioso","Academico", "Social", "Salud"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERCATEGORIAS);
        txtIcategoria.setAdapter(arrayAdapter);
        txtIcategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoria = txtIcategoria.getItemAtPosition(position).toString();
               ////// System.out.println(categoria+"---------------------------------");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        showDialog();


        tokenManager = TokenManager.getInstance(this.getSharedPreferences("pref",MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class,tokenManager);

        /*if (getSupportActionBar() != null)
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_done);

        // Creación del fragmento de inserción
        if (savedInstanceState == null) {

            String h = getIntent().getExtras().getString(Constantes.ID_USER);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_crud, InsertEventFragment.newInstance(h), "InsertFragment")
                    .commit();
        }*/
        txtIrespondable.getEditText().setText(persistenUserSesion.getAccesUsuario().getName());
        btnRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }
    void register(){



        String name = txtIname.getEditText().getText().toString();

        String descripcion = txtIdescripcion.getEditText().getText().toString();
        String horar = Integer.toString(hora);
        String punto_encuentro = txtIpunto_encuentro.getEditText().getText().toString();
        String respondable = txtIrespondable.getEditText().getText().toString();
        String fecha_inicio = fechaInicioIt.getText().toString();
        String fecha_fin = fechaFinIt.getText().toString();
        String imagen_evento = txtIimagen_evento.getEditText().getText().toString();
        String precio = txtIprecio.getEditText().getText().toString();
        String idcast = persistenUserSesion.getAccesUsuario().getId();
        int id_guia = Integer.parseInt(idcast);

        //
        txtIname.setError(null);
        txtIcategoria.setError(null);
        txtIdescripcion.setError(null);
        txtIpunto_encuentro.setError(null);
        txtIrespondable.setError(null);
        //txtIfecha_inicio.setError(null);
        txtIimagen_evento.setError(null);
        txtIprecio.setError(null);
        Evento evento = new Evento(id_guia,name,categoria,descripcion,horar,fecha_inicio,fecha_fin,precio,punto_encuentro,respondable);
        System.out.println(categoria+"++++++++");
        //txtIfecha_fin.setError(null);

       // if(validator.validate()) {
            call = service.store(id_guia,name,categoria,descripcion,horar,fecha_inicio,fecha_fin,precio,punto_encuentro,respondable);
            call.enqueue(new Callback<ResponseInsert>() {
                @Override
                public void onResponse(Call<ResponseInsert> call, Response<ResponseInsert> response) {
                    Log.w(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {

                        showLoginExitoso(response.body().getData());
                        startActivity(new Intent(InsertActivity.this,OrganizadorMainActivity.class));
                        finish();


                    } else {
                       // handleErrors(response.errorBody());
                        showLoginError(response.message());

                    }
                }

                @Override
                public void onFailure(Call<ResponseInsert> call, Throwable t) {
                    Snackbar.make(mInsertFormView, "ErrorConectiom", Snackbar.LENGTH_LONG).show();
                    Log.w(TAG, "onFailure: " + t.getMessage());

                }
            });
       // }
    }
    private void showLoginError(String error) {
        Snackbar.make(mInsertFormView, error, Snackbar.LENGTH_LONG).show();
    }
    private void showLoginExitoso(String respuesta) {
        Snackbar.make(mInsertFormView, "Registro Exitoso" , Snackbar.LENGTH_LONG).show();
    }

    public void setTokenManager(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    public void showDialog(){

        fechaInicioIt = findViewById(R.id.pubEve_txt_fechaI);
        fechaInicioLbl =  findViewById(R.id.pubEve_lbl_fechaI);
        fechaFinIt =  findViewById(R.id.pubEve_txt_fechaF);
        fechaFinLbl =  findViewById(R.id.pubEve_lbl_fechaF);

        fechaInicioLbl.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(1);
                    }
                }
        );
        fechaFinLbl.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(2);
                    }
                }
        );
    }

    @Override
    protected Dialog onCreateDialog(int id){

        if(id == 1)
            return  new DatePickerDialog(this,dpickerListenrI, anio,mes,dia);
        else
            return  new DatePickerDialog(this,dpickerListenrF, anio,mes,dia);
    }

    private DatePickerDialog.OnDateSetListener dpickerListenrI = new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            anio = year;
            mes = month;
            dia = dayOfMonth;

            fechaInicioIt.setText(dayOfMonth+"/"+(month+1)+"/"+year);

        }
    };

    private DatePickerDialog.OnDateSetListener dpickerListenrF = new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            anio = year;
            mes = month;
            dia = dayOfMonth;

            fechaFinIt.setText(dayOfMonth+"/"+(month+1)+"/"+year);


        }
    };


    @Override
    public void onClick(View v) {

        if (v==horaLbl){
            final Calendar c= Calendar.getInstance();
            hora=c.get(Calendar.HOUR_OF_DAY);
            minutos=c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    horaE.setText(hourOfDay+":"+minute);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }
    }
}
