package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by linuxp on 11/03/18.
 */

public class Comentario {


    //<editor-fold defaultstate="collapsed" desc="Atributos de la clase">

    private String idComentario;
    private String idOrganizador;
    private String idAsistente;
    private String nombreComento;
    private String fechaComentario;
    private String descripicionComentario;
    private String calificacionComentario;


//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">

    public Comentario() {

    }

    public Comentario(String idComentario, String idOrganizador, String idAsistente, String nombreComento, String fechaComentario, String descripicionComentario, String calificacionComentario) {
        this.idComentario = idComentario;
        this.idOrganizador = idOrganizador;
        this.idAsistente = idAsistente;
        this.nombreComento = nombreComento;
        this.fechaComentario = fechaComentario;
        this.descripicionComentario = descripicionComentario;
        this.calificacionComentario = calificacionComentario;
    }





    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">

    public String getIdComentario() {
        return idComentario;
    }

    public String getIdOrganizador() {
        return idOrganizador;
    }

    public String getIdAsistente() {
        return idAsistente;
    }

    public String getNombreComento() {
        return nombreComento;
    }

    public String getFechaComentario() {
        return fechaComentario;
    }

    public String getDescripicionComentario() {
        return descripicionComentario;
    }

    public String getCalificacionComentario() {
        return calificacionComentario;
    }




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">
    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }

    public void setIdOrganizador(String idOrganizador) {
        this.idOrganizador = idOrganizador;
    }

    public void setIdAsistente(String idAsistente) {
        this.idAsistente = idAsistente;
    }

    public void setNombreComento(String nombreComento) {
        this.nombreComento = nombreComento;
    }

    public void setFechaComentario(String fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public void setDescripicionComentario(String descripicionComentario) {
        this.descripicionComentario = descripicionComentario;
    }

    public void setCalificacionComentario(String calificacionComentario) {
        this.calificacionComentario = calificacionComentario;
    }




    //</editor-fold>

}
