package com.px.linuxp.eventpopayan.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Modelo.Comentario;
import com.px.linuxp.eventpopayan.R;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by linuxp on 11/03/18.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder>
        implements ItemClickListener{

    private List<Comentario> items;

    /*
    Contexto donde actua el recycler view
     */
    private Context context;


    public CommentsAdapter(List<Comentario> items, Context context) {

        this.context = context;
        this.items = items;

    }


    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_vista_comentarios_organizador, viewGroup, false);
        return new CommentsAdapter.CommentsViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder viewHolder, int i) {
        viewHolder.comentNombre.setText(items.get(i).getNombreComento());
        viewHolder.comentDescripcion.setText(items.get(i).getDescripicionComentario());
        viewHolder.comentCalificación.setNumStars(2);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public static class CommentsViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView comentNombre;
        public TextView comentDescripcion;
        public MaterialRatingBar comentCalificación;



        public ItemClickListener listener;

        public CommentsViewHolder(View v, ItemClickListener listener) {
            super(v);
            comentNombre = (TextView) v.findViewById(R.id.itemComOrg_txt_nombre_org);
            comentDescripcion = (TextView) v.findViewById(R.id.itemComOrg_txt_apreciacion);
            comentCalificación = (MaterialRatingBar) v.findViewById(R.id.itemComOrg_rtb_calificacion);
            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }
}
