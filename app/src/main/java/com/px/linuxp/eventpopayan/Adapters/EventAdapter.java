package com.px.linuxp.eventpopayan.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Actividades.GUI.DetailActivity;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.R;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import java.util.List;

/**
 * Created by linuxp on 3/11/17.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder>
        implements ItemClickListener {


    /**
     * Lista de objetos {@link Evento} que representan la fuente de datos
     * de inflado
     */
    private List<EventoSugar> items;

    /*
    Contexto donde actua el recycler view
     */
    private Context context;


    public EventAdapter(List<EventoSugar> items, Context context) {

        this.context = context;
        this.items = items;

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_eventos_asistentes, viewGroup, false);
        return new EventViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(EventViewHolder viewHolder, int i) {
        viewHolder.mtaTitulo.setText(items.get(i).getName());

        //viewHolder.mtaDireccion.setText(items.get(i).getPunto_encuentro());
        //viewHolder.mtaTipo.setText(items.get(i).getTipoevento());
        viewHolder.mtaPrecio.setText(items.get(i).getPrecio());
        viewHolder.mtaFechaI.setText(items.get(i).getFecha_inicio());
        viewHolder.mtaFechaF.setText(items.get(i).getFecha_fin());


    }

    /**
     * Sobrescritura del método de la interfaz {@link ItemClickListener}
     *
     * @param view     item actual
     * @param position posición del item actual
     */
    @Override
    public void onItemClick(View view, int position) {
        DetailActivity.launch(
                (Activity) context, new Long( items.get(position).getId()));
    }


    public static class EventViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView mtaTitulo;
        //public TextView mtaTipo;
        public TextView mtaPrecio;
        //public TextView mtaDireccion;
        public TextView mtaFechaI;
        public TextView mtaFechaF;


        public ItemClickListener listener;

        public EventViewHolder(View v, ItemClickListener listener) {
            super(v);
            mtaTitulo = (TextView) v.findViewById(R.id.iea_CV_nombre);
           // mtaTipo = (TextView) v.findViewById(R.id.iea_CV_Categoria);
            mtaFechaI = (TextView) v.findViewById(R.id.iea_CV_fecha_inicio);
            mtaFechaF = (TextView) v.findViewById(R.id.iea_Cv_fecha_final);
            mtaPrecio = (TextView) v.findViewById(R.id.iea_Cv_precio);
            //mtaDireccion = (TextView) v.findViewById(R.id.id_CV_direccion);

            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }
}







