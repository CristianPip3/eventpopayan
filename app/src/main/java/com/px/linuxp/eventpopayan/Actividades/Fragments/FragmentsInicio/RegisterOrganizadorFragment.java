package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PublicFragment;
import com.px.linuxp.eventpopayan.R;

/**
 * Created by linuxp on 26/01/18.
 */

public class RegisterOrganizadorFragment extends PublicFragment{
    public RegisterOrganizadorFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
