package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by user on 05/09/2017. 05
 */

public class MensajeRecibir extends Mensaje {
    //<editor-fold defaultstate="collapsed" desc="Atributos de la clase">
    private Long hora;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">
    public MensajeRecibir() {
    }

    public MensajeRecibir(Long hora) {
        this.hora = hora;
    }

    public MensajeRecibir(String idMensaje, String nombre, String mensaje, String fechaMensaje, String tipoMensaje, Long hora) {
        super(idMensaje, nombre, mensaje, fechaMensaje, tipoMensaje);
        this.hora = hora;
    }




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">
    public Long getHora() {
        return hora;
    }




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">
    public void setHora(Long hora) {
        this.hora = hora;
    }




    //</editor-fold>

}
