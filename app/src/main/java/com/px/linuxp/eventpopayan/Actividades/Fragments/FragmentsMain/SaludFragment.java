package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.TokenManager;
import com.px.linuxp.eventpopayan.Adapters.EventAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 5/04/18.
 */

public class SaludFragment extends PublicFragment{
    private static final String TAG = PopularesFragment.class.getSimpleName();
    public static ArrayList<Evento> misEventos =new ArrayList<Evento>();
    ApiService service;
    TokenManager tokenManager;
    Call<PostResponse> call;



    /*
    Adaptador del recycler view
     */
    private EventAdapter adapter;

    /*
    Instancia global del recycler view
     */
    private RecyclerView lista;
    private List<Evento> eventosda = new ArrayList<>();

    /*
    instancia global del administrador
     */
    private RecyclerView.LayoutManager lManager;
    String id="";
    public SaludFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.content_user, container, false);
        lista =  rootView.findViewById(R.id.id_main_recicler_usr);
        lista.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);
        //eventosG();
        //getEvents();
        obtenerEventos();


        return rootView;
    }








    @Override
    public void procesarRespuesta(List<EventoSugar> response) {
        super.procesarRespuesta( response);
/*

        List<Evento> eventos = new ArrayList<>();
        eventos.addAll(response.body().getData());*/





        adapter = new EventAdapter(response,getActivity());
        // Setear adaptador a la lista
        lista.setAdapter(adapter);
    }
    public void obtenerEventos(){

        List<EventoSugar> listSugar = EventoSugar.find(EventoSugar.class,"Categoria = ?","Salud");//(EventoSugar.
        if (!listSugar.isEmpty()){
            System.out.println(listSugar.get(1).getId()+"PASAMOS");
            procesarRespuesta(listSugar);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(call != null){
            call.cancel();
            call = null;
        }
    }

}

