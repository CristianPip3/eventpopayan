package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by linuxp on 4/11/17.
 */

public class Mensaje {

    //<editor-fold defaultstate="collapsed" desc="Atributos de la clase">
    private String idMensaje;
    private String nombre;
    private String mensaje;
    private String fechaMensaje;
    private String tipoMensaje;




//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">


    public Mensaje(String idMensaje, String nombre, String mensaje, String fechaMensaje, String tipoMensaje) {
        this.idMensaje = idMensaje;
        this.nombre = nombre;
        this.mensaje = mensaje;
        this.fechaMensaje = fechaMensaje;
        this.tipoMensaje = tipoMensaje;
    }

    public Mensaje() {
    }




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">
    public String getIdMensaje() {
        return idMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getFechaMensaje() {
        return fechaMensaje;
    }

    public String getTipoMensaje() {
        return tipoMensaje;
    }



    public String getNombre() {
        return nombre;
    }







    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">




    //</editor-fold>


}
