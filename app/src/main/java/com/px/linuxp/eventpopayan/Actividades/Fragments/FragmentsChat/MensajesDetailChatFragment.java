package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsChat;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.px.linuxp.eventpopayan.Actividades.Fragments.InsertEventFragment;
import com.px.linuxp.eventpopayan.Adapters.MensajeAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.Modelo.Mensaje;
import com.px.linuxp.eventpopayan.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MensajesDetailChatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MensajesDetailChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MensajesDetailChatFragment extends Fragment {
    private static final String TAG = InsertEventFragment.class.getSimpleName();
    private TextView nombreDetailChat;
    private TextView mensajeDetailChat;
    private Button btnEnviarDetailChat;
    private ImageView imgFotoUsuarioDetailChat;
    private ImageButton imgButonFotoChat;
    private String idUsuario;
    private MensajeAdapter adapter;
    private RecyclerView lista;
    private RecyclerView.LayoutManager lManager;
    private FirebaseDatabase database;
    DatabaseReference databaseReference;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private String mParam1;


    private OnFragmentInteractionListener mListener;

    public MensajesDetailChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param idUsuario Parameter 1.
     * @return A new instance of fragment MensajesDetailChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MensajesDetailChatFragment newInstance(String idUsuario) {
        MensajesDetailChatFragment fragment = new MensajesDetailChatFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.ID_USER, idUsuario);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mensajes__detail, container, false);
        idUsuario = getArguments().getString(Constantes.ID_USER);
        nombreDetailChat = (TextView)view.findViewById(R.id.txt_nombre_datil_msj_chat);
        mensajeDetailChat = (TextView)view.findViewById(R.id.txt_mensaje_detail_chat);
        btnEnviarDetailChat = (Button)view.findViewById(R.id.btn_enviar_msj_chat);
        imgButonFotoChat = (ImageButton)view.findViewById(R.id.btn_imagen_detail_chat);
        imgFotoUsuarioDetailChat = (ImageView)view.findViewById(R.id.perfil_usr_datil_msj_chat);
        lista = (RecyclerView)view.findViewById(R.id.rvMensajes_datail_msj);
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("chat");
        adapter = new MensajeAdapter(getActivity());
        lista.setHasFixedSize(true);
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);
        lista.setAdapter(adapter);



        btnEnviarDetailChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.push().setValue(new Mensaje("1","Prueba",mensajeDetailChat.getText().toString(),"18:59","2"));
                mensajeDetailChat.setText("");

            }
        });

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollbar();
            }
        });




        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Mensaje mensaje = dataSnapshot.getValue(Mensaje.class);
                //adapter.llenarMensjaR();
                adapter.addMensaje(mensaje);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        return view;
    }
    private void setScrollbar(){
        lista.scrollToPosition(adapter.getItemCount()-1);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
