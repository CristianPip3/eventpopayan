package com.px.linuxp.eventpopayan.Modelo;

import java.util.Map;

/**
 * Created by user on 05/09/2017. 05
 */

public class MensajeEnviar extends Mensaje {
    //<editor-fold defaultstate="collapsed" desc="Atributos de la clase">
    private Map hora;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">
    public MensajeEnviar() {
    }

    public MensajeEnviar(Map hora) {
        this.hora = hora;
    }


    public MensajeEnviar(String idMensaje, String nombre, String mensaje, String fechaMensaje, String tipoMensaje, Map hora) {
        super(idMensaje, nombre, mensaje, fechaMensaje, tipoMensaje);
        this.hora = hora;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">
    public Map getHora() {
        return hora;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">
    public void setHora(Map hora) {
        this.hora = hora;
    }




    //</editor-fold>

}
