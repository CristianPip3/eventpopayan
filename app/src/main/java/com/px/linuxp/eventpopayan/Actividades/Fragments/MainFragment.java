package com.px.linuxp.eventpopayan.Actividades.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.LoginActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.OrganizadorController;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.TokenManager;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PublicFragment;
import com.px.linuxp.eventpopayan.Adapters.EventAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 3/11/17.
 */

public class MainFragment extends PublicFragment {

    private static final String TAG = MainFragment.class.getSimpleName();

    private EventAdapter adapter;
    private RecyclerView lista;
    private View mProgressView;
    private RelativeLayout relativeLayoutContainer;
    private View mLoginFormView;
    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager lManager;
    String id="";

    public MainFragment() {
        super();
    }

    public static MainFragment createInstance(String id_usuario) {
        MainFragment detailFragmentUser = new MainFragment();
        return detailFragmentUser;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.content_org, container, false);
        lista =  v.findViewById(R.id.id_main_recicler_org);
        lista.setHasFixedSize(true);
        relativeLayoutContainer = v.findViewById(R.id.container_org);
        mLoginFormView = v.findViewById(R.id.id_main_recicler_org);
                // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);
        //mProgressView = v.findViewById(R.id.event_progress_o);
            obtenerEventos();




        //eventosOrganizador();



        return v;
    }
    public void obtenerEventos(){


        List<EventoSugar> listSugar = EventoSugar.listAll(EventoSugar.class);//(EventoSugar.
        if (!listSugar.isEmpty()){
            procesarRespuesta(listSugar);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    private void showLoading(){
        TransitionManager.beginDelayedTransition(relativeLayoutContainer);
        mLoginFormView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
    }
    private void showForm(){
        TransitionManager.beginDelayedTransition(relativeLayoutContainer);
        mLoginFormView.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.GONE);
    }
    @Override
    public void procesarRespuesta(List<EventoSugar> response) {
        super.procesarRespuesta( response);

        adapter = new EventAdapter(response,getActivity());

        lista.setAdapter(adapter);
/*

        List<Evento> eventos = new ArrayList<>();
        eventos.addAll(response.body().getData());
        adapter = new EventAdapter(eventos,getActivity());
        // Setear adaptador a la lista
        lista.setAdapter(adapter);*/
    }




}


