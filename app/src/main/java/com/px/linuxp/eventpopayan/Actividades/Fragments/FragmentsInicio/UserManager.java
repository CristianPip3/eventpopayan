package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Context;

import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Herramientas.Entites.UserResponse;
import com.px.linuxp.eventpopayan.Modelo.UserResponseUsuario;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by linuxp on 2/04/18.
 */

public class UserManager {
    private static final String TAG = UserManager.class.getSimpleName();
    ApiService serviceAut;
    TokenManager tokenManager;
    public static  boolean tipo = true;
    Call<UserResponse> call;
    UserResponseUsuario user;
    PersistenUserSesion persistenUserSesion;

    public UserManager() {
    }

    public UserManager( ApiService serviceAut, TokenManager tokenManager, Call<UserResponse> call,UserResponseUsuario user) {
        this.serviceAut = serviceAut;
        this.tokenManager = tokenManager;
        this.call = call;
        this.user = user;
    }
    public  boolean revisarPerfil(final Context context/*, final OnUsuarioResponse callbak*/){
        final boolean bandera = false;
        persistenUserSesion = new PersistenUserSesion(context);
        tokenManager = TokenManager.getInstance(context.getSharedPreferences("pref",MODE_PRIVATE));
        serviceAut = RetrofitBuilder.createServiceWithAuth(ApiService.class,tokenManager);
        call = serviceAut.profile();
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){


                }else {

                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
        return bandera;
    }





    public  void procesarRespuesta(Response<UserResponse> response){
        //response.body().guardarUsuario();


    }


    public static boolean isTipo() {
        return tipo;
    }

    public static void setTipo(boolean tipo) {
        UserManager.tipo = tipo;
    }

    public UserResponseUsuario getUser() {
        return user;
    }

    public void setUser(UserResponseUsuario user) {
        this.user = user;
    }
}
/*mapearUsuario(response.body().getUsuario().is_org(),
                            response.body().getUsuario().getCedula(),
                            response.body().getUsuario().getFoto_perfil(),
                            response.body().getUsuario().getCreated_at(),
                            response.body().getUsuario().getCf_idm(),
                            response.body().getUsuario().is_admin(),
                            response.body().getUsuario().getTj_pro(),
                            response.body().getUsuario().getDescripcion_perfil(),
                            response.body().getUsuario().getUpdated_at(),
                            response.body().getUsuario().getGenero(),
                            response.body().getUsuario().getName(),
                            response.body().getUsuario().getId(),
                            response.body().getUsuario().getTelefono(),
                            response.body().getUsuario().getEmail());










                            */