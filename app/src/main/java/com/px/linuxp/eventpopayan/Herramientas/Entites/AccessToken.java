package com.px.linuxp.eventpopayan.Herramientas.Entites;

import com.squareup.moshi.Json;

/**
 * Created by linuxp on 27/01/18.
 */

public class AccessToken {
    @Json(name = "token_type")
    String tokenType;
    @Json(name = "expire_in")
    int expiresIn;
    @Json(name = "access_token")
    String accessToken;
    @Json(name = "refresh_token")
    String refreshToken;
    /*
    @Json(name = "tipo_usr")
    int tipo_usr;

    public int getTipo_usr() {
        return tipo_usr;
    }

    public void setTipo_usr(int tipo_usr) {
        this.tipo_usr = tipo_usr;
    }*/

    public String getTokenType() {
        return tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
