package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by linuxp on 20/11/17.
 */

public class Usuario extends Persona {
    public Usuario() {
        super();
    }

    public Usuario(int id, String nombre, String email) {
        super(id, nombre, email);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String email) {
        super.setEmail(email);
    }
}
