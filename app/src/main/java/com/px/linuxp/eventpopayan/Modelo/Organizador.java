package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by linuxp on 20/03/18.
 */

public class Organizador extends Persona {
    private boolean organizador;
    private String cedula;
    private String genero;
    private String descripcionPerfil;
    private String telefono;
    private String tj_pro;
    private String cf_idm;

    public Organizador() {
        super();
    }

    public Organizador(int id, String name, String email) {
        super(id, name, email);
    }

    public Organizador(int id, String name, String email, boolean organizador, String cedula, String genero, String descripcionPerfil, String telefono, String tj_pro, String cf_idm) {
        super(id, name, email);
        this.organizador = organizador;
        this.cedula = cedula;
        this.genero = genero;
        this.descripcionPerfil = descripcionPerfil;
        this.telefono = telefono;
        this.tj_pro = tj_pro;
        this.cf_idm = cf_idm;
    }


    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String email) {
        super.setEmail(email);
    }

    public boolean isOrganizador() {
        return organizador;
    }

    public void setOrganizador(boolean organizador) {
        this.organizador = organizador;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDescripcionPerfil() {
        return descripcionPerfil;
    }

    public void setDescripcionPerfil(String descripcionPerfil) {
        this.descripcionPerfil = descripcionPerfil;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTj_pro() {
        return tj_pro;
    }

    public void setTj_pro(String tj_pro) {
        this.tj_pro = tj_pro;
    }

    public String getCf_idm() {
        return cf_idm;
    }

    public void setCf_idm(String cf_idm) {
        this.cf_idm = cf_idm;
    }

}
