package com.px.linuxp.eventpopayan.Adapters;

/**
 * Created by Ujang Wahyu on 24/01/2017.
 */


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.px.linuxp.eventpopayan.R;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PopularesFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.RecrativosFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.ReligiosoFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.SocialFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.AcademicosFragment;


/**
 * Created by Ujang Wahyu on 18/08/2016.
 */

public class TabAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles ={"A","B","C","D"};
    int[] icon = new int[]{R.mipmap.ic_chrome_reader_mode_black_24dp,R.mipmap.ic_message_black_24dp,R.mipmap.ic_contact_mail_black_24dp,R.mipmap.ic_supervisor_account_black_24dp};
    private int heightIcon;

    public TabAdapter(FragmentManager fm, Context c){
        super(fm);
        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon=(int)(24*scale+0.5f);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag= null;

        if(position ==0){
            frag = new PopularesFragment();
        }else if(position == 1){
            frag = new RecrativosFragment();
        }else if(position == 2){
            frag = new ReligiosoFragment();
        }else if(position == 3){
            frag = new SocialFragment();
        }else if(position == 4){
            frag = new AcademicosFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    public CharSequence getPageTitle(int position){
        Drawable d = mContext.getResources().getDrawable(icon[position]);
       d.setBounds(0,0,heightIcon,heightIcon);
       ImageSpan is = new ImageSpan(d);

        SpannableString sp = new SpannableString(" ");
        sp.setSpan(is,0,sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sp;
    }

}