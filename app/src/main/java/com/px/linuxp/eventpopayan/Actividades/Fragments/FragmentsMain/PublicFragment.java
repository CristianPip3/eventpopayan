package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;

import java.util.List;

import retrofit2.Response;

/**
 * Created by linuxp on 15/01/18.
 */

public abstract class PublicFragment extends Fragment {


    public PublicFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }
    public void getEvents(){




    }
    public void procesarRespuesta(List<EventoSugar> list){


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
