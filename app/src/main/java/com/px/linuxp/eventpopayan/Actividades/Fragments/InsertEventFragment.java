package com.px.linuxp.eventpopayan.Actividades.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InsertEventFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InsertEventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InsertEventFragment extends Fragment {
    private static final String TAG = InsertEventFragment.class.getSimpleName();

    private EditText tituloIt;
    private EditText descripcionIt;
    private TextView fechaInicioIt;
    private TextView fechaFinIt;
    private EditText horaInicioIt;
    private EditText direccionUrlIt;
    private Spinner tipoEventoIt;
    private EditText precioEIt;
    private EditText puntoEncuentroIt;
    private FloatingActionButton btnAdjuntoViewIt;
    private String idUsuario;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    // TODO: Rename and change types of parameters


    private OnFragmentInteractionListener mListener;

    public InsertEventFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id_usuario Parameter 1.
     * @return A new instance of fragment InsertEventFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InsertEventFragment newInstance(String id_usuario) {
        InsertEventFragment fragmentInsert = new InsertEventFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.ID_USER,id_usuario);
        fragmentInsert.setArguments(args);
        return fragmentInsert;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           // idUsuario = getArguments().getString(ARG_PARAM1);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.actualizareventos, container, false);
        /*idUsuario = getArguments().getString(Constantes.ID_USER);
        tituloIt = (EditText) view.findViewById(R.id.txt_titulo_input_it);
        descripcionIt = (EditText) view.findViewById(R.id.txt_descripcion_input_it);
        fechaInicioIt = (TextView) view.findViewById(R.id.fecha_inicio_input_it);
        fechaFinIt = (TextView) view.findViewById(R.id.txt_fechaf_imput);
        horaInicioIt = (EditText) view.findViewById(R.id.txt_hora_inicio_imput);
        direccionUrlIt = (EditText) view.findViewById(R.id.txt_descripcion_input_it);
        tipoEventoIt = (Spinner) view.findViewById(R.id.spnr_categoria_input_it);
        precioEIt = (EditText) view.findViewById(R.id.edt_precio_imput_it);
        puntoEncuentroIt = (EditText) view.findViewById(R.id.edt_pto_encuentro_imput_it);
        btnAdjuntoViewIt = (FloatingActionButton) view.findViewById(R.id.fab_imput_it_gallery);

*/
        return view;



    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
