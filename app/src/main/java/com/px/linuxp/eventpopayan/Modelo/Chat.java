package com.px.linuxp.eventpopayan.Modelo;

import java.util.ArrayList;

/**
 * Created by linuxp on 4/11/17.
 */

public class Chat {
    //<editor-fold defaultstate="collapsed" desc="Atributos de la clase">
    private int idChat;
    private String nombreUsuario;
    private String ultimoMensaje;
    private String fechaUltimoMsj;
    private String urlFoto;


    //private ArrayList<Mensaje> mensajes;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">
    public Chat() {
    }

    public Chat(int idChat,String nombreUsuario,String ultimoMensaje, String fechaUltimoMsj,String urlFoto /*,ArrayList<Mensaje> mensajes*/) {
        this.idChat = idChat;
        this.nombreUsuario = nombreUsuario;
        this.ultimoMensaje = ultimoMensaje;
        this.fechaUltimoMsj = fechaUltimoMsj;
        this.urlFoto = urlFoto;
       // this.mensajes = mensajes;
    }

    public String getUltimoMensaje() {
        return ultimoMensaje;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">
    public int getIdChat() {
        return idChat;
    }
    public String getNombreUsuario() {
        return nombreUsuario;
    }


    public String getUrlFoto() {
        return urlFoto;
    }

    public String getFechaUltimoMsj() {
        return fechaUltimoMsj;
    }






    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">




    //</editor-fold>


}
