package com.px.linuxp.eventpopayan.Actividades.GUI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsChat.ChatActivityFragment;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.R;


public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Toolbar toolbar = findViewById(R.id.toolbarchat);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null)
           // getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_done);

        // Creación del fragmento de inserción
        if (savedInstanceState == null) {

            String h = getIntent().getExtras().getString(Constantes.ID_USER);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.id_container_chat, ChatActivityFragment.newInstance(h), "ChatFragment")
                    .commit();
        }

    }

}
