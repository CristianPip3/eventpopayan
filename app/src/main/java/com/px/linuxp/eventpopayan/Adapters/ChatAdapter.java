package com.px.linuxp.eventpopayan.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Actividades.GUI.DetailActivity;
import com.px.linuxp.eventpopayan.Modelo.Chat;
import com.px.linuxp.eventpopayan.Modelo.Mensaje;
import com.px.linuxp.eventpopayan.R;

import java.util.List;

/**
 * Created by linuxp on 4/11/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder>
        implements ItemClickListener {

    /**
     * Lista de objetos {@link Chat} que representan la fuente de datos
     * de inflado
     */
    private List<Chat> items;

    /*
    Contexto donde actua el recycler view
     */
    private Context context;


    public ChatAdapter(List<Chat> items, Context context) {

        this.context = context;
        this.items = items;

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ChatAdapter.ChatViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_chat, viewGroup, false);
        return new ChatAdapter.ChatViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ChatViewHolder viewHolder, int i) {
        viewHolder.mtaNombre.setText(items.get(i).getNombreUsuario());
        viewHolder.mtaRecibio.setText(items.get(i).getFechaUltimoMsj());
        viewHolder.mtaMsj.setText(items.get(i).getUltimoMensaje());



    }

    /**
     * Sobrescritura del método de la interfaz {@link ItemClickListener}
     *
     * @param view     item actual
     * @param position posición del item actual
     */
    @Override
    public void onItemClick(View view, int position) {
        DetailActivity.launch(
                (Activity) context, items.get(position).getIdChat());
    }


    public static class ChatViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView mtaNombre;
        public TextView mtaMsj;
        public TextView mtaRecibio;



        public ItemClickListener listener;

        public ChatViewHolder(View v, ItemClickListener listener) {
            super(v);
            mtaNombre = (TextView) v.findViewById(R.id.id_CVC_nombre_usr);
            mtaMsj = (TextView) v.findViewById(R.id.id_CVC_mensaje);
            mtaRecibio = (TextView) v.findViewById(R.id.id_CVC_fecha_e);
            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }
}


