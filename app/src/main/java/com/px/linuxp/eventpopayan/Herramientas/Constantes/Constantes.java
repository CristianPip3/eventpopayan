package com.px.linuxp.eventpopayan.Herramientas.Constantes;

/**
 * Created by linuxp on 3/11/17.
 */

public class Constantes {
    /**
     * Transición Home -> Detalle
     */
    public static  final String ID_USER= "id_usuario";
    public static  final String ID_CHAT="id_chat";

    public static final int CODIGO_DETALLE = 100;
    public static final Boolean CODIGO_SESION = false;

    /**
     * Transición Detalle -> Actualización
     */
    public static final int CODIGO_ACTUALIZACION = 101;

    public static final int CODIGO_DETALLE_USUARIO = 102;
    public static final int CAPTURA =106;

    /**
     * Puerto que utilizas para la conexión.
     * Dejalo en blanco si no has configurado esta carácteristica.
     */
    private static final String PUERTO_HOST = ":63343";
    /**
     * Dirección IP de genymotion o AVD
     */
    private static final String IP = "10.73.70.44";
    /**
     * URLs del Web Service
     */
    public static final String REGISTOUSUARIO = "http://"+ IP +"/IWish/quejate/InsertUsuario.php";
    public static final String INICIOSESION =   "http://"+ IP +"/IWish/quejate/LoginVolley.php";
    public static final String GET = "http://" + IP +"/IWish/quejate/GetPqrs.php";
    public static final String GET_BY_ID = "http://" + IP +"/IWish/quejate/DetallesPqrs.php";
    public static final String UPDATE = "http://" + IP +"/IWish/quejate/UpdatePqrs.php";
    public static final String DELETE = "http://" + IP +"/IWish/quejate/DeletePqr.php";
    public static final String INSERT = "http://" + IP +"/IWish/quejate/InsertPqrs.php";
    public static final String GET_BY_ID_USUARIO = "http://" + IP +"/IWish/quejate/DetallesUsuario.php";
    public static final String GET_MY_PQRS ="http://"+ IP+"/IWish/quejate/getPqrsUsuario.php";


    /**
     * Clave para el valor extra que representa al identificador de una meta
     */

    public static final String EXTRA_ID = "IDEXTRA";
    public static final String EXTRA_ID_CHAT = "IDEXTRACHAT";
}
