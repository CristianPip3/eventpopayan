package com.px.linuxp.eventpopayan.Herramientas.Entites;

import com.px.linuxp.eventpopayan.Modelo.UserResponseUsuario;

public class UserResponse {

    private UserResponseUsuario usuario;

    public UserResponseUsuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UserResponseUsuario usuario) {
        this.usuario = usuario;
    }
}
