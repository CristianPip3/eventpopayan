package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Herramientas.Entites.UserResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.ResponseInsert;
import com.px.linuxp.eventpopayan.Modelo.UserResponseUsuario;
import com.px.linuxp.eventpopayan.R;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 11/03/18.
 */

public class PerfilActivity extends AppCompatActivity {

    private static final String TAG = PerfilActivity.class.getSimpleName();
    ApiService serviceAut;
    ApiService serviceAut1;

    TokenManager tokenManager;
    public static  boolean tipo = true;
    Call<ResponseInsert> call;
    Call<UserResponse> call1;
    UserResponseUsuario user;
    PersistenUserSesion persistenUserSesion;

    private View mInsertFormView;
    private TextInputLayout txtInameP;
    private TextInputLayout txtIIdentificacionP;
    private TextInputLayout txtIEmailP;
    private TextInputLayout txtIpuntoDescripcionP;
    private MaterialSpinner txtIIGenero;
    private TextInputLayout txtITelefonoP;
    private Button btnRegistarP;
    private String genero;




    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actualizar_perfil_asistente2);
        mInsertFormView = findViewById(R.id.ViewPrincipal_Acp);
        txtInameP = findViewById(R.id.txtI_nombre_p);
        txtIIdentificacionP = findViewById(R.id.txtI_identificacion_p);
        txtIEmailP = findViewById(R.id.txtI_email_P);
        txtIpuntoDescripcionP = findViewById(R.id.txtI_descripcionP);
        btnRegistarP = findViewById(R.id.btn_actualizar_perfilasistente);
        persistenUserSesion = new PersistenUserSesion(this);
        tokenManager = TokenManager.getInstance(this.getSharedPreferences("pref",MODE_PRIVATE));
        serviceAut = RetrofitBuilder.createServiceWithAuth(ApiService.class,tokenManager);
        String[] SPINNERGENERO = {"Masculino", "Femenino"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERGENERO);


        txtIIGenero = findViewById(R.id.spinner_genero_asistnte);
        txtITelefonoP = findViewById(R.id.txtITelefono_p);
        txtIIGenero.setAdapter(arrayAdapter);
        txtIIGenero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genero = txtIIGenero.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cargarDatos();
        btnRegistarP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });






    }
    public  boolean revisarPerfil(){
        final boolean bandera = false;
        persistenUserSesion = new PersistenUserSesion(this);
        tokenManager = TokenManager.getInstance(this.getSharedPreferences("pref",MODE_PRIVATE));
        serviceAut = RetrofitBuilder.createServiceWithAuth(ApiService.class,tokenManager);
        call1 = serviceAut1.profile();
        call1.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    carDatos(response);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {


            }
        });
        return bandera;
    }
    public void carDatos(Response<UserResponse> response){
        txtInameP.getEditText().setText(response.body().getUsuario().getName());
        txtIEmailP.getEditText().setText(response.body().getUsuario().getEmail());
        txtIEmailP.getEditText().setEnabled(false);
        txtIIdentificacionP.getEditText().setText(response.body().getUsuario().getCedula());
        txtIpuntoDescripcionP.getEditText().setText(response.body().getUsuario().getDescripcion_perfil());
        txtITelefonoP.getEditText().setText(response.body().getUsuario().getTelefono());




    }

    public void cargarDatos(){

        txtInameP.getEditText().setText(persistenUserSesion.getAccesUsuario().getName());
        txtIEmailP.getEditText().setText(persistenUserSesion.getAccesUsuario().getEmail());

        txtIEmailP.getEditText().setEnabled(false);
        txtIIdentificacionP.getEditText().setText(persistenUserSesion.getAccesUsuario().getCedula());

    }
    public void isRol(){
        if(tokenManager.isLoadedData()){
            register();
        }else {
            registerOrg();
        }
    }
    void register(){
        String name = txtInameP.getEditText().getText().toString();
        String cedula = txtIIdentificacionP.getEditText().getText().toString();
        String descripcion = txtIpuntoDescripcionP.getEditText().getText().toString();
        String email = txtIEmailP.getEditText().getText().toString();
        String telefono = txtITelefonoP.getEditText().getText().toString();
        String idcast = persistenUserSesion.getAccesUsuario().getId();
        int id = Integer.parseInt(idcast);


        call = serviceAut.updateUser(id,name,cedula,descripcion,telefono,genero);
        call.enqueue(new Callback<ResponseInsert>() {
            @Override
            public void onResponse(Call<ResponseInsert> call, Response<ResponseInsert> response) {
                Log.w(TAG, "Actualizooo " + response);
                if (response.isSuccessful()) {
                    Log.w(TAG, "onResponse: " + response);


                    showLoginExitoso(response.body().getData());


                } else {
                    // handleErrors(response.errorBody());
                    showLoginError(response.message());

                }
            }

            @Override
            public void onFailure(Call<ResponseInsert> call, Throwable t) {
                Snackbar.make(mInsertFormView, "ErrorConectiom", Snackbar.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());

            }
        });
        // }*/
    }
    void registerOrg(){
        String name = txtInameP.getEditText().getText().toString();
        String cedula = txtIIdentificacionP.getEditText().getText().toString();
        String descripcion = txtIpuntoDescripcionP.getEditText().getText().toString();
        String email = txtIEmailP.getEditText().getText().toString();
        String telefono = txtITelefonoP.getEditText().getText().toString();
        String idcast = persistenUserSesion.getAccesUsuario().getId();
        int id = Integer.parseInt(idcast);


        call = serviceAut.updateOrg(id,name,cedula,descripcion,telefono,genero);
        call.enqueue(new Callback<ResponseInsert>() {
            @Override
            public void onResponse(Call<ResponseInsert> call, Response<ResponseInsert> response) {
                Log.w(TAG, "Actualizooo " + response);
                if (response.isSuccessful()) {
                    Log.w(TAG, "onResponse: " + response);

                    showLoginExitoso(response.body().getData());
                } else {
                    // handleErrors(response.errorBody());
                    showLoginError(response.message());

                }
            }

            @Override
            public void onFailure(Call<ResponseInsert> call, Throwable t) {
                Snackbar.make(mInsertFormView, "ErrorConectiom", Snackbar.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());

            }
        });
        // }*/
    }
    private void showLoginError(String error) {
        Snackbar.make(mInsertFormView, error, Snackbar.LENGTH_LONG).show();
    }
    private void showLoginExitoso(String respuesta) {
        Snackbar.make(mInsertFormView, "Registro Exitoso" , Snackbar.LENGTH_LONG).show();
    }


}
