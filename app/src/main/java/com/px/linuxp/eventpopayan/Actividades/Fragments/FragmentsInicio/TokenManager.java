package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.SharedPreferences;

import com.px.linuxp.eventpopayan.Herramientas.Entites.AccessToken;

/**
 * Created by linuxp on 1/03/18.
 */

public class TokenManager {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private static final String ORG = "findOrg";

    private static TokenManager INSTANCE = null;

    private TokenManager(SharedPreferences prefs){
        this.prefs = prefs;
        this.editor = prefs.edit();
    }

    static synchronized TokenManager getInstance(SharedPreferences prefs){
        if(INSTANCE == null){
            INSTANCE = new TokenManager(prefs);
        }
        return INSTANCE;
    }
    /**/
    public void setOrganizador(boolean is_org){
        editor.putBoolean(ORG,is_org).commit();
    }
    private SharedPreferences getSharedPreferences() {
        return prefs;
    }
    public Boolean isLoadedData(){
        return getSharedPreferences().getBoolean(ORG, false);
    }
    /**/

    public void saveToken(AccessToken token){
        editor.putString("ACCESS_TOKEN", token.getAccessToken()).commit();
        editor.putString("REFRESH_TOKEN", token.getRefreshToken()).commit();
    }

    public void deleteToken(){
        editor.remove("ACCESS_TOKEN").commit();
        editor.remove("REFRESH_TOKEN").commit();
        editor.remove(ORG).commit();
    }

    public AccessToken getToken(){
        AccessToken token = new AccessToken();
        token.setAccessToken(prefs.getString("ACCESS_TOKEN", null));
        token.setRefreshToken(prefs.getString("REFRESH_TOKEN", null));
        return token;
    }
}
