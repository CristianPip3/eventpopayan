package com.px.linuxp.eventpopayan.Actividades.SectionAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.AcademicosFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PopularesFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.RecrativosFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.ReligiosoFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.SaludFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.SocialFragment;

/**
 * Created by linuxp on 26/01/18.
 */

public class SectionsPagerAdapter extends SectionsPagerAdapterA  {



    public SectionsPagerAdapter(FragmentManager fm ) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag= null;
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position ==0){
           // mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.primaryDarkColor));

            frag = new PopularesFragment();
        }else if(position == 1){
           // mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.Assisten));
            frag = new RecrativosFragment();

        }else if(position == 2){
          // mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.chat));
            frag = new SocialFragment();
        }else if(position == 3){
          // mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.comentarios));
            frag = new SaludFragment();
        }else if(position == 4){
          //  mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.detalles));
            frag = new AcademicosFragment();

        }
        else if(position == 5){
            //  mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.detalles));
            frag = new ReligiosoFragment();
        }
        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "GASTRONOMICO";
            case 1:
                return "RECREATIVOS";
            case 2:
                return "SOCIALES";
            case 3:
                return "SALUD";
            case 4:
                return "ACADEMICOS";
            case 5:
                return "RELIGIOSOS";
        }
        return null;
    }






}
