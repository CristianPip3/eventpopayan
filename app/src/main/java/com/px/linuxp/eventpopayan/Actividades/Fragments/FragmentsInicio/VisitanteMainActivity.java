package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.px.linuxp.eventpopayan.Actividades.SectionAdapters.SectionsPagerAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.R;

/**
 * Created by linuxp on 2/04/18.
 */

public class VisitanteMainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    private ViewPager mViewPager;
    private Toolbar toolbar;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FloatingSearchView mFloatingSearchView;
    private AppBarLayout mAppBarLayout;
    private UserController userController;
    private OrganizadorController organizadorController;
    int i;
    TokenManager tokenManager;
    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_vist);
        Intent intent= getIntent();
         i = intent.getFlags();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        //mFloatingSearchView =  findViewById(R.id.floating_search_view);
        mAppBarLayout =  findViewById(R.id.appbar_visitante);

        // Set up the ViewPager with the sections adapter.
        mViewPager =  findViewById(R.id.container_visitante);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(i);
        userController = new UserController();

        TabLayout tabLayout =  findViewById(R.id.tabs_visit);
        System.out.println(i+"PREBAaaaaaaaaaaaaa");
        tabLayout.setSelectedTabIndicatorHeight(i);


        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        userController.ObtenerEventos();

    }





    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()){
            case 0:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.populares));

                break;
            case 1:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.recreativos));
                break;
            case 2:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.socilaes));
                break;
            case 3:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.salud));
                break;
            case 4:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.academicos));
                break;
            case 5:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.religiosos));
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }



}
