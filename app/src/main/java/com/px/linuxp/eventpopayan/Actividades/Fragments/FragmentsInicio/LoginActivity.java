package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.px.linuxp.eventpopayan.Actividades.GUI.Registro;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Herramientas.Entites.AccessToken;
import com.px.linuxp.eventpopayan.Herramientas.Entites.ApiError;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Persistencia.Utils;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final String TAG = "LoginActivity";


    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private View mProgressView;
    private View mLoginFormView;
    private FloatingActionButton fbButtomRegister;
    private SharedPreferences prefs;
    private LinearLayout relativeLayoutContainer;
    private PersistenUserSesion persistenUserSesion;
    private ImageButton imButtonFace;
    UserManager userManager;
    ApiService service;
    Call<AccessToken> call;
    AwesomeValidation validator;
    TokenManager tokenManager;
    FacebookManager facebookManager;
    ConnectReceiver connectReceiver;
    IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio_sesion);
        // Set up the login form.
        mEmailView = findViewById(R.id.ses_edt_correo);
       // imButtonFace = findViewById(R.id.is_btn_facebook);
        tilEmail = findViewById(R.id.is_til_email);
        tilPassword = findViewById(R.id.is_til_password);
        //fbButtomRegister = findViewById(R.id.is_fab_register);
        mPasswordView = (EditText) findViewById(R.id.password);
        service = RetrofitBuilder.createService(ApiService.class);
        prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        userManager = new UserManager();
        intentFilter = new IntentFilter();
        intentFilter.addAction(ServiceConect.terminoOk);
        intentFilter.addAction(ServiceConect.terminoError);
        connectReceiver = new ConnectReceiver();
        registerReceiver(connectReceiver, intentFilter);
        tokenManager = TokenManager.getInstance(prefs);
        facebookManager = new FacebookManager(service, tokenManager);
        persistenUserSesion = new PersistenUserSesion(LoginActivity.this);

        //validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        //setupRules();
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (!isOnline()) {
                        showLoginError(getString(R.string.error_network));
                        return false;
                    }

                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        if(tokenManager.getToken().getAccessToken() != null){
            if (tokenManager.isLoadedData()){
                startActivity(new Intent(this, OrganizadorMainActivity.class));
                finish();
            }else{
                startActivity(new Intent(this, UserMainActivity.class));
                finish();
            }
        }

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOnline()) {
                    showLoginError(getString(R.string.error_network));
                    return;
                }
                attemptLogin();
            }
        });

       /* imButtonFace.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFacebook();
            }
        });*/



        relativeLayoutContainer = findViewById(R.id.is_container);

        mLoginFormView = findViewById(R.id.email_login_form);
        mProgressView = findViewById(R.id.login_progress);

    }
    public void registro(){
        Intent intent = new Intent(this,Registro.class);
        startActivity(intent);
        finish();
    }
    private void showLoading(){
        TransitionManager.beginDelayedTransition(relativeLayoutContainer);
        mLoginFormView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
    }
    private void showForm(){
        TransitionManager.beginDelayedTransition(relativeLayoutContainer);
        mLoginFormView.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.GONE);
    }
    public void loginFacebook(){
        showLoading();
        facebookManager.login(this, new FacebookManager.FacebookLoginListener() {

            @Override
            public void onSuccess() {
                Log.w(TAG, "Pasamos: " );

                facebookManager.clearSession();
                startActivity(new Intent(LoginActivity.this, OrganizadorMainActivity.class));
                finish();
            }

            @Override
            public void onError(String message) {
                showForm();
                Log.w(TAG, "Error " );
                showLoginError(message);


            }
        });

    }


    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;
        //validator.clear();

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //if(validator.validate()){
            boolean salir;
                showLoading();
                call = service.login(email,password);
                call.enqueue(new Callback<AccessToken>() {
                    @Override
                    public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                        showProgress(false);
                        Log.w(TAG, "Pasamos: " + response);

                        if(response.isSuccessful()){
                            tokenManager.saveToken(response.body());
                            hacerLogin();

                        }else{

                            if (response.code()==422){
                                handleErrors(response.errorBody());

                            }
                            if(response.code()==401){
                                ApiError apiError = Utils.converErrors(response.errorBody());
                                showLoginError(apiError.getMessage());


                            }
                            showForm();

                        }

                    }

                    @Override
                    public void onFailure(Call<AccessToken> call, Throwable t) {
                        showLoginError("Error en el Servidor");
                        Log.w(TAG, "onFailure: " + t.getMessage());
                        showForm();

                    }
                });


            //}




        }



    }
    private void handleErrors(ResponseBody response){


        ApiError apiError = Utils.converErrors(response);

        for (Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if (error.getKey().equals("username")) {
                tilEmail.setError(error.getValue().get(0));
            }
            if (error.getKey().equals("password")) {
                tilPassword.setError(error.getValue().get(0));
            }
        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }
    private void showAppointmentsScreen()  {


        if (tokenManager.isLoadedData()){
            Log.w(TAG, "ORGANIZADOR" );
            //Log.w(TAG, "SUGAR ORM "+ persistenUserSesion.getAccesUsuario().getName());
            //Log.w(TAG, "SUGAR ORM "+ persistenUserSesion.getAccesUsuario().getId());
            startActivity(new Intent(this, OrganizadorMainActivity.class));
            finish();
        }else{
            Log.w(TAG, "USUARIO" );
            //Log.w(TAG, "SUGAR ORM "+ u.getId());
           // showLoginError(u.getName());
            startActivity(new Intent(this, UserMainActivity.class));
            finish();
        }


        //UserManager.isTipo();
        /*UserResponseUsuario user = UserResponseUsuario.findById(UserResponseUsuario.class,1);
        Log.w(TAG, "SUGAR ORM " + user.getEmail()+user.getName());

        if(user.is_org() && userManager.getUser() !=null){
            startActivity(new Intent(this, OrganizadorMainActivity.class));
            finish();

        }else if (!user.is_org() && userManager.getUser() !=null) {
            startActivity(new Intent(this, UserMainActivity.class));
            finish();

        }*/


    }
    public  void hacerLogin(){
        Intent intent = new Intent(this,ServiceConect.class);
        startService(intent);

    }
    private void showLoginError(String error) {
        Snackbar.make(mLoginFormView, error, Snackbar.LENGTH_LONG).show();
    }
    public void setupRules() {

        validator.addValidation(this, R.id.is_til_email, Patterns.EMAIL_ADDRESS, R.string.err_email);
        validator.addValidation(this, R.id.is_til_password, RegexTemplate.NOT_EMPTY, R.string.err_password);
    }
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(call != null){
            call.cancel();
            call = null;
        }

    }
    public class ConnectReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            switch(intent.getAction()){
                case ServiceConect.terminoOk:
                    showAppointmentsScreen();
                    break;
                case ServiceConect.terminoError:

                    break;


            }
            unregisterReceiver(connectReceiver);
        }
    }

}