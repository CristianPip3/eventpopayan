package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Context;
import android.content.Intent;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;

import com.px.linuxp.eventpopayan.Adapters.EventAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.Modelo.NewEventoData;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by linuxp on 22/03/18.
 */

public class OrganizadorController {
    private static final String TAG = "OrganizadorController";
    List<NewEventoData> eventos = new ArrayList<>();;
    Context context;
    private Fragment mainFragment;
    private ActivityCompat organizadorMainActivity;

    ApiService service;
    TokenManager tokenManager;
    Call<PostResponse> call;
     EventAdapter adapter;


    public OrganizadorController() {
    }

    public OrganizadorController(final Context context) {

        this.tokenManager = TokenManager.getInstance(context.getSharedPreferences("pref",MODE_PRIVATE));
        this.service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
        this.tokenManager = tokenManager;
    }
    public void eventosOrganizador(final Context context ){
        //tokenManager = TokenManager.getInstance(context.getSharedPreferences("pref",MODE_PRIVATE));
        //service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
        call = service.posts();
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                Log.w(TAG, "onResponse: Respuesta " + response );

                if(response.isSuccessful()){
                    procesarRespuesta(response);
                }else {
                    tokenManager.deleteToken();
                    context.startActivity(new Intent(context, LoginActivity.class));
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage() );
            }
        });



    }
    public void procesarRespuesta(Response<PostResponse> response) {
        for (NewEventoData evento:response.body().getData()

                ) {
            EventoSugar eventoSugar = new EventoSugar(evento);
            eventoSugar.save();
        }
    }
    public void leerComentarios(){}
    public void realizarEvento(){}
    public void iniciarChat(){}

    public List<NewEventoData> getEventos() {
        return eventos;
    }


    public EventAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(EventAdapter adapter) {
        this.adapter = adapter;
    }
}
