package com.px.linuxp.eventpopayan.Actividades.SectionAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.RegisterAssitentFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.RegisterOrganizadorFragment;

/**
 * Created by linuxp on 26/01/18.
 */

public class SectiontsPagerAdapterRegister extends SectionsPagerAdapterA {
    public SectiontsPagerAdapterRegister(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Registro Asistente";

        }
        return null;

    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag= null;
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position ==0){
            // mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.primaryDarkColor));

            frag = new RegisterAssitentFragment();
        }
        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);
        return frag;
    }
}
