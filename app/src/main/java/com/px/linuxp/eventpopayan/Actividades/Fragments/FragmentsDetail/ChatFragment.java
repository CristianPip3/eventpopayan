package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.px.linuxp.eventpopayan.R;

/**
 * Created by linuxp on 15/01/18.
 */

public class ChatFragment extends Fragment {
    public ChatFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_chat, container, false);
        return rootView;
    }
}
