package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.stream.JsonReader;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.LoginActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.OrganizadorController;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.TokenManager;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.UserController;
import com.px.linuxp.eventpopayan.Adapters.EventAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 15/01/18.
 */

public class PopularesFragment extends PublicFragment {
    private static final String TAG = PopularesFragment.class.getSimpleName();
    public static ArrayList<Evento> misEventos =new ArrayList<Evento>();
    Call<PostResponse> call;
    SwipeRefreshLayout swipeRefreshLayout;
    /*
    Adaptador del recycler view
     */
    private EventAdapter adapter;
    /*
    Instancia global del recycler view
     */
    private RecyclerView lista;
    private List<Evento> eventosda = new ArrayList<>();
    /*
    instancia global del administrador
     */
    private RecyclerView.LayoutManager lManager;
    String id="";
    public PopularesFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_user, container, false);
        lista =  rootView.findViewById(R.id.id_main_recicler_usr);
        lista.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);
        //eventosG();
        //getEvents();
        swipeRefreshLayout = rootView.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

               EventoSugar.deleteAll(EventoSugar.class);
                obtenerEventos();



            }

        });

        if(!swipeRefreshLayout.isRefreshing())
            obtenerEventos();





        return rootView;
    }




    @Override
    public void procesarRespuesta(List<EventoSugar> response) {
        super.procesarRespuesta( response);


        adapter = new EventAdapter(response,getActivity());

        lista.setAdapter(adapter);
    }

    public void obtenerEventos(){


        List<EventoSugar> listSugar = EventoSugar.find(EventoSugar.class,"Categoria = ?","Gastronomia");//(EventoSugar.
        if (!listSugar.isEmpty()){
            procesarRespuesta(listSugar);
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(call != null){
            call.cancel();
            call = null;
        }
    }

}
/*
public void procesarRespuesta() {
        Evento evento = new Evento("1","Noche para nada", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Cultural","Gratis","Cra 4 # 5-17");
        Evento evento1 = new Evento("1","Fiesta Halloweb", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Social","Gratis","Cra 4 # 5-17");
        Evento evento2 = new Evento("1","Visita Volcan", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Natural","Gratis","Cra 4 # 5-17");
        Evento evento3 = new Evento("1","Visita Presidente", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Cultural","$5.000","Cra 4 # 5-17");
        Evento evento4 = new Evento("1","Festejo Fin de Año", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Cultural","$10.000","Cra 4 # 5-17");
        Evento evento5 = new Evento("1","Hackaton Cafetera", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Ciencia","$2.000","Cra 4 # 5-17");
        Evento evento6 = new Evento("1","Apertura de PizzaP la mejor desición", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Cultural","$20.000","Cra 4 # 5-17");
        Evento evento7 = new Evento("1","Celebración 200 años", "Noche de museos es un evento Gratis","03-11-2017","04-11-2017","8:00","wwww.alcaldia.com","Cultural","Gratis","Cra 4 # 5-17");

        List<Evento> eventos = new ArrayList<>();
        eventos.add(evento);
        eventos.add(evento1);
        eventos.add(evento2);
        eventos.add(evento3);
        eventos.add(evento5);
        eventos.add(evento5);
        eventos.add(evento6);
        eventos.add(evento7);




        adapter = new EventAdapter(eventos,getActivity());
        // Setear adaptador a la lista
        lista.setAdapter(adapter);
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

*
*public List<Evento> readJsonStream() throws IOException{
        JsonReader reader;

        try {
            return leerArrayAnimales(reader);
        }finally {
            read
        }

    }
    public List leerArrayAnimales(JsonReader reader) throws IOException {
        // Lista temporal
        ArrayList animales = new ArrayList();

        reader.beginArray();
        while (reader.hasNext()) {
            // Leer objeto
            animales.add(leerAnimal(reader));
        }
        reader.endArray();
        return animales;
    }
    public Evento leerAnimal(JsonReader reader) throws IOException {
        String especie = null;
        String descripcion = null;
        String imagen = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case "especie":
                    especie = reader.nextString();
                    break;
                case "descripcion":
                    descripcion = reader.nextString();
                    break;
                case "imagen":
                    imagen = reader.nextString();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return new Evento(especie, descripcion, imagen);
    }
*
*
* */
