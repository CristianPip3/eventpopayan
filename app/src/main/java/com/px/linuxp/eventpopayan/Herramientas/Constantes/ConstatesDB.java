package com.px.linuxp.eventpopayan.Herramientas.Constantes;

/**
 * Created by linuxp on 20/11/17.
 */

public class ConstatesDB {

    /*Versión y Nombre de la Base de datos*/

    //<editor-fold defaultstate="collapsed" desc="Nombre y Versión de la Base de Datos">
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "myBd.db";



    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tabla Usuario">
    public static final String TABLA_USUARIO = "Usuario";
    public static final String COLUMNA_USUARIO_ID = "_id";
    public static final String COLUMNA_NOMBRE = "nombre";
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tabla Eventos">

    public static final String TABLA_EVENTOS ="eventos";
    public static final String COLUMNA_EVENTO_ID = "idEvento";
    public static final String COLUMNA_TITULO_EVENTO = "titulo";
    public static final String COLUMNA_DESCRIPCION_EVENTO = "descripcion";
    public static final String COLUMNA_FECHA_INICIO_EVENTO = "fechaiEvento";
    public static final String COLUMNA_FECHA_FIN_EVENTO = "fechafEvento";
    public static final String COLUMNA_HORA_EVENTO = "horaEvento";
    public static final String COLUMNA_URL_EVENTO = "url";
    public static final String COLUMNA_TIPO_EVENTO = "tipoEvento";
    public static final String COLUMNA_PRECIO_EVENTO = "precioEvento";
    public static final String COLUMNA_PUNTO_ENCUENTRO_E ="ptoEncuentro";




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tabla Chat">




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Tabla Registros">




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Crear Tabla USUARIO">
    private  static final  String SQL__CREAR_ENTIDAD = "create_table"
            + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + "integer primary key autoincrement, ";




    //</editor-fold>


}
