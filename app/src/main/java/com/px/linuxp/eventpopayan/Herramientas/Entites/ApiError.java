package com.px.linuxp.eventpopayan.Herramientas.Entites;

import java.util.List;
import java.util.Map;

/**
 * Created by linuxp on 1/03/18.
 */

public class ApiError {

    String message;
    Map<String, List<String>> errors;

    public String getMessage() {
        return message;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
