package com.px.linuxp.eventpopayan.Modelo;

import android.content.Intent;

import com.orm.SugarRecord;

/**
 * Created by linuxp on 12/04/18.
 */

public class EventoSugar extends SugarRecord {
    private int id_guia;
    private String name;
    private String categoria;
    private String descripcion;
    private String hora;
    private String punto_encuentro;
    private String respondable;
    private String fecha_inicio;
    private String imagen_evento;
    private String precio;
    private String fecha_fin;
    private Long id;




    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">


    public EventoSugar() {
    }
    public EventoSugar(NewEventoData e){

        this.id_guia = e.getId_guia();
        this.name = e.getName();
        this.categoria = e.getCategoria();
        this.descripcion = e.getDescripcion();
        this.hora = e.getHora();
        this.fecha_inicio = e.getFecha_inicio();
        this.fecha_fin = e.getFecha_fin();
        this.precio = e.getPrecio();
        this.punto_encuentro = e.getLugar_evento();
        this.respondable = e.getResponsable();
        this.setId(e.getId());

    }

    public EventoSugar(int id_guia, String name, String categoria, String descripcion, String hora, String fecha_inicio, String fecha_fin, String precio, String punto_encuentro, String respondable) {
        this.id_guia = id_guia;
        this.name = name;
        this.categoria = categoria;
        this.descripcion = descripcion;
        this.hora = hora;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.precio = precio;
        this.punto_encuentro = punto_encuentro;
        this.respondable = respondable;

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        super.setId(id);
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setId_guia(int id_guia) {

        this.id_guia = id_guia;
    }

    public String getImagen_evento() {
        return imagen_evento;
    }

    public void setImagen_evento(String imagen_evento) {
        this.imagen_evento = imagen_evento;
    }

    public String getRespondable() {
        return respondable;
    }

    public void setRespondable(String respondable) {
        this.respondable = respondable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get de la clase">
    public int getId_guia(){

        return id_guia;
    }
    public String getName() {
        return name;
    }



    public String getDescripcion() {
        return descripcion;
    }
    public String getHora() {
        return hora;
    }




    public String getFecha_inicio() {
        return fecha_inicio;
    }



    public String getFecha_fin() {
        return fecha_fin;
    }






    public String getPrecio() {
        return precio;
    }



    public String getPunto_encuentro() {
        return punto_encuentro;
    }





    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set de la Clase ">

    public void setName(String name) {
        this.name = name;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setHora(String hora) {
        this.hora = hora;
    }
    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


    public void setPunto_encuentro(String punto_encuentro) {
        this.punto_encuentro = punto_encuentro;
    }
}
