package com.px.linuxp.eventpopayan.Actividades.GUI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail.AssistendFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail.ChatFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail.ComentFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.OrganizadorMainActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.UserMainActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.InsertEventFragment;
import com.px.linuxp.eventpopayan.Actividades.Fragments.MainFragment;
import com.px.linuxp.eventpopayan.Adapters.TabAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Tab.SlidingTabLayout;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.R;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail.DetailEventFragment;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;

/**
 * Created by linuxp on 3/11/17.
 */

public class DetailActivity extends AppCompatActivity  implements OnTabSelectListener{
            /**
             * The {@link ViewPager} that will host the section contents.
             */

    private BottomBar bottomBar;
    private Toolbar mToolbar;
    //private BottomNavigationView mbottomNavigationView;

    private ViewPager mViewPagerDetail;
    //private SectionsPagerAdapterDetail mSectionPagerAdapterDetail;
    private SlidingTabLayout mSlidingTabLayout;


    /*
     * Instancia global de la meta a detallar
     */
    private Long id_evento;


    /**
     * Inicia una nueva instancia de la actividad
     *
     * @param activity Contexto desde donde se lanzará
     * @param id_pqrs   Identificador de la meta a detallar
     */
    public static void launch(Activity activity, long id_pqrs) {
        Intent intent = getLaunchIntent(activity, id_pqrs);
        System.out.println(id_pqrs+"**************");
        activity.startActivityForResult(intent, Constantes.CODIGO_DETALLE);

    }

    /**
     * Construye un Intent a partir del contexto y la actividad
     * de detalle.
     *activity.startActivityForResult(intent, Constantes.CODIGO_DETALLE);
     * @param context Contexto donde se inicia
     * @param id_pqrs  Identificador de la meta
     * @return Intent listo para usar
     */
    public static Intent getLaunchIntent(Context context, long id_pqrs) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(Constantes.EXTRA_ID, id_pqrs);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        bottomBar = (BottomBar) findViewById(R.id.bottombar);
        mToolbar = (Toolbar) findViewById(R.id.toolboar_fragmentDetail);
        //ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_IMAGE);



        //mbottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomnavigation);
        //mSectionPagerAdapterDetail = new SectionsPagerAdapterDetail(getSupportFragmentManager());
       // mViewPagerDetail = (ViewPager) findViewById(R.id.container_page_detail);
        //mViewPagerDetail.setAdapter(new TabAdapter(getSupportFragmentManager(), this));
       // mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
        //TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsdetail);
        //tabLayout.setupWithViewPager(mViewPagerDetail);
        //mSlidingTabLayout.setViewPager(mViewPagerDetail);


        if (getSupportActionBar() != null) {
            // Dehabilitar titulo de la actividad
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            // Setear ícono "X" como Up button
            //getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setSupportActionBar(mToolbar);

        // Retener instancia
        if (getIntent().getLongExtra(Constantes.EXTRA_ID,-1l) != -1l)
            id_evento = getIntent().getLongExtra(Constantes.EXTRA_ID,-1l);
        System.out.println(id_evento+"+++++++++++++++");
        /*if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_detail, DetailEventFragment.createInstance(id_evento),
                            "DetailFragment")
                    .commit();
        }*/
        bottomBar.setOnTabSelectListener(this);




        /*bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            Fragment fragment = null;
            String nombreFragmento ="";

            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){
                    case R.id.tab_detalles:
                        fragment = new ComentFragment();
                        nombreFragmento ="DetailFragment";
                        break;
                    case R.id.tab_coment:
                        fragment = new ComentFragment();
                        nombreFragmento ="ComentDetailFragment";
                        break;
                    case R.id.tab_char:
                        fragment = new ChatFragment();
                        nombreFragmento ="ChatDetailFragment";
                        break;
                    case R.id.tab_Assit:
                        fragment = new AssistendFragment();
                        nombreFragmento ="AssitDetailFragment";
                        break;
                    default:
                        fragment = new DetailEventFragment();
                        nombreFragmento ="DetailFragment";
                        break;



                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_page_detail,fragment,
                                nombreFragmento)
                        .commit();


            }
        });

        ___________________________
        mbottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                String nombreFragmento ="";
                switch (item.getItemId()){
                    case R.id.menu_details:
                        fragment = new ComentFragment();
                        nombreFragmento ="DetailFragment";
                        break;
                    case R.id.menu_coment:
                        fragment = new ComentFragment();
                        nombreFragmento ="ComentDetailFragment";
                        break;
                    case R.id.menu_chat:
                        fragment = new ChatFragment();
                        nombreFragmento ="ChatDetailFragment";
                        break;
                    case R.id.menu_asistend:
                        fragment = new AssistendFragment();
                        nombreFragmento ="AssitDetailFragment";
                        break;
                    default:
                        fragment = new DetailEventFragment();
                        nombreFragmento ="DetailFragment";
                        break;



                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_detail,fragment,
                                nombreFragmento)
                        .commit();
                return true;


            }
        });
        __________________________-*/

    }
    /*private void viewDialog(){


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.publicar_comentario, null);
        mBuilder.setView(mView);
        Button buttonCrearCuenta = mView.findViewById(R.id.rol_btn_reg_org);
        Button buttonUser = mView.findViewById(R.id.rol_btn_reg_asis);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        buttonCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publicarComentario;
                dialog.dismiss();

            }
        });




    }*/

    @Override
    public void onTabSelected(@IdRes int tabId) {
        Fragment fragment = null;
        String nombreFragmento ="";
        switch (tabId){
            case R.id.tab_detalles:
                fragment = DetailEventFragment.createInstance(id_evento);
                nombreFragmento ="DetailFragment";
                break;
            case R.id.tab_coment:
                fragment = new ComentFragment();
                nombreFragmento ="ComentDetailFragment";
                break;
            case R.id.tab_char:
                fragment = new ChatFragment();
                nombreFragmento ="ChatDetailFragment";
                break;
            case R.id.tab_Assit:
                fragment = new AssistendFragment();
                nombreFragmento ="AssitDetailFragment";
                break;
            default:
                fragment = new DetailEventFragment();
                nombreFragmento ="DetailFragment";
                break;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_detail,fragment,
                        nombreFragmento)
                .commit();

    }
}
/**
 *
 *
 *
 *
 *
 *
 *
 */

