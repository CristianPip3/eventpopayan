package com.px.linuxp.eventpopayan.Actividades.GUI;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import com.orm.SugarContext;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.LoginActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.OrganizadorMainActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.UserMainActivity;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.VisitanteController;
import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.VisitanteMainActivity;
import com.px.linuxp.eventpopayan.R;

import java.security.MessageDigest;

/**
 * Created by linuxp on 3/03/18.
 */




public class InicioActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButtonPopular;
    private Button mButtonSocial;
    private Button mButtonRecreativo;
    private Button mButtonReligioso;
    private Button mButtonSalud;
    private Button mButtonAcademico;
    private Button mButtonIngresar;
    private Button mButtonRegistrarse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista_principal);
        System.out.print("KeyHashes"+KeyHashes());
        mButtonPopular = findViewById(R.id.pri_btn_popular);
        mButtonIngresar = findViewById(R.id.pri_btn_ingresar);
        mButtonRegistrarse = findViewById(R.id.pri_btn_registrar);
        mButtonRecreativo = findViewById(R.id.pri_btn_recreativo);
        mButtonReligioso = findViewById(R.id.pri_btn_religioso);
        mButtonSalud = findViewById(R.id.pri_btn_salud);
        mButtonSocial = findViewById(R.id.pri_btn_social);
        mButtonAcademico = findViewById(R.id.pri_btn_academico);
        mButtonRegistrarse = findViewById(R.id.pri_btn_registrar);

        mButtonPopular.setOnClickListener(this);
        mButtonSocial.setOnClickListener(this);
        mButtonRecreativo.setOnClickListener(this);
        mButtonReligioso.setOnClickListener(this);
        mButtonSalud.setOnClickListener(this);
        mButtonAcademico.setOnClickListener(this);
        mButtonIngresar.setOnClickListener(this);
        mButtonRegistrarse.setOnClickListener(this);
        SugarContext.init(this);






    }

    public String KeyHashes(){
        PackageInfo info;
        String KeyHashes = null;
        try {
            info = getPackageManager().getPackageInfo("com.px.linuxp.eventpopayan", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures){
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                KeyHashes = new String(Base64.encode(md.digest(),0));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return KeyHashes;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        boolean bander = false;
        switch (v.getId()){
            case R.id.pri_btn_popular:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                bander =true;
                intent.setFlags(0);
                break;
            case R.id.pri_btn_social:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                bander =true;
                intent.setFlags(2);
                break;
            case R.id.pri_btn_recreativo:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                bander =true;
                intent.setFlags(1);
                break;
            case R.id.pri_btn_religioso:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                bander =true;
                intent.setFlags(5);
                break;
            case R.id.pri_btn_salud:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                intent.setFlags(3);
                bander =true;
                break;
            case R.id.pri_btn_academico:
                intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
                intent.setFlags(4);
                bander =true;
                break;
            case R.id.pri_btn_ingresar:
                bander =true;
                intent.setClass(InicioActivity.this,LoginActivity.class);

                break;
            case R.id.pri_btn_registrar:
                bander = true;
                intent.setClass(InicioActivity.this,Registro.class);
                break;


        }
        if(bander){
            //intent.setClass(InicioActivity.this, VisitanteMainActivity.class);
            startActivity(intent);
            finish();

        }else{
            viewDialog();
        }


    }
    

    private void viewDialog(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(InicioActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.crear_cuenta, null);
        mBuilder.setView(mView);
        Button buttonCrearCuenta = mView.findViewById(R.id.rol_btn_reg_org);
        Button buttonUser = mView.findViewById(R.id.rol_btn_reg_asis);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        buttonCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InicioActivity.this, Registro.class);
                startActivity(intent);
                dialog.dismiss();

            }
        });


    }


}
