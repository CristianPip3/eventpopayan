package com.px.linuxp.eventpopayan.Herramientas.Constantes;

import android.content.Context;
import android.content.SharedPreferences;

import com.px.linuxp.eventpopayan.Modelo.UserResponseUsuario;

/**
 * Created by linuxp on 11/04/18.
 */

public class PersistenUserSesion {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context context;
    private static final String NAME_FILE_SHARED_PREFERENCE = "loadDataPreference";
    private static final String CREDENCIAL ="loaded_data";
    private static final String ID_USUARIO = "IDUSUARIO";
    private static final String CEDULA= "findCedula";
    private static final String NAME= "findNombre";
    private static final String EMAIL= "findEmail";
    private static final String ORG = "findOrg";



    private String foto_perfil;
    private String created_at;
    private String cf_idm;
    private boolean is_admin;
    private String tj_pro;
    private String descripcion_perfil;
    private String updated_at;
    private String genero;
    private String name;
    private String id;
    private String telefono;
    private String email;

    public PersistenUserSesion(Context context) {
        this.prefs = context.getSharedPreferences(NAME_FILE_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        this.editor = prefs.edit();
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        return prefs;
    }
    public Boolean isLoadedData(){
        return getSharedPreferences().getBoolean(ORG, false);
    }
    public void setOrganizador(boolean is_org){
        editor.putBoolean(ORG,is_org).commit();
    }
    public void setLoadedData(boolean loadedData,String idUsuario,String cedula, String email, String name ){
        editor.putBoolean(CREDENCIAL, loadedData).commit();
        editor.putString(CEDULA,cedula).commit();
        editor.putString(EMAIL,email).commit();
        editor.putString(NAME,name).commit();
        editor.putString(ID_USUARIO,idUsuario).commit();

    }
    public void deleteSesion(){
        editor.remove(CREDENCIAL).commit();
        editor.remove(CEDULA).commit();
        editor.remove(EMAIL).commit();
        editor.remove(NAME).commit();
        editor.remove(ID_USUARIO).commit();
    }
    public UserResponseUsuario getAccesUsuario(){
        UserResponseUsuario user = new UserResponseUsuario();
        user.setIs_org(prefs.getBoolean(CREDENCIAL,false));
        user.setEmail(prefs.getString(EMAIL,null));
        user.setCedula(prefs.getString(CEDULA,null));
        user.setName(prefs.getString(NAME,null));
        user.setId(prefs.getString(ID_USUARIO,null));
        return  user;


    }


}
