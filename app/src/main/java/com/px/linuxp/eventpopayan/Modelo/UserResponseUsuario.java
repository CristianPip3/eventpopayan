package com.px.linuxp.eventpopayan.Modelo;



public class UserResponseUsuario   {

    private boolean is_org;
    private String cedula;
    private String foto_perfil;
    private String created_at;
    private String cf_idm;
    private boolean is_admin;
    private String tj_pro;
    private String descripcion_perfil;
    private String updated_at;
    private String genero;
    private String name;
    private String id;
    private String telefono;
    private String email;

    public UserResponseUsuario() {
    }

    public UserResponseUsuario (boolean is_org, String cedula, String foto_perfil, String created_at, String cf_idm, boolean is_admin, String tj_pro, String descripcion_perfil, String updated_at, String genero, String name,String id,  String telefono, String email) {
        this.is_org = is_org;
        this.cedula = cedula;
        this.foto_perfil = foto_perfil;
        this.created_at = created_at;
        this.cf_idm = cf_idm;
        this.is_admin = is_admin;
        this.tj_pro = tj_pro;
        this.descripcion_perfil = descripcion_perfil;
        this.updated_at = updated_at;
        this.genero = genero;
        this.name = name;
        this.id = id;
        this.telefono = telefono;
        this.email = email;
    }

    public boolean is_org() {
        return is_org;
    }

    public void setIs_org(boolean is_org) {
        this.is_org = is_org;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFoto_perfil() {
        return foto_perfil;
    }

    public void setFoto_perfil(String foto_perfil) {
        this.foto_perfil = foto_perfil;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCf_idm() {
        return cf_idm;
    }

    public void setCf_idm(String cf_idm) {
        this.cf_idm = cf_idm;
    }

    public boolean is_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public String getTj_pro() {
        return tj_pro;
    }

    public void setTj_pro(String tj_pro) {
        this.tj_pro = tj_pro;
    }

    public String getDescripcion_perfil() {
        return descripcion_perfil;
    }

    public void setDescripcion_perfil(String descripcion_perfil) {
        this.descripcion_perfil = descripcion_perfil;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
