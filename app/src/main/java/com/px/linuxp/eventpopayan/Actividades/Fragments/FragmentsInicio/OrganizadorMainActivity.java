package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.px.linuxp.eventpopayan.Actividades.Fragments.MainFragment;
import com.px.linuxp.eventpopayan.Actividades.GUI.ChatActivity;
import com.px.linuxp.eventpopayan.Actividades.Perfil.Organizador.ComentariosActivity;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.Organizador;
import com.px.linuxp.eventpopayan.R;

import java.util.List;


public class OrganizadorMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "OrganizadorMainActivity";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    //private ViewPager mViewPager;
    private Toolbar toolbar;
    private List<Evento> eventoList;
    //private SectionsPagerAdapter mSectionsPagerAdapter;
    private FloatingSearchView mFloatingSearchView;
    private AppBarLayout mAppBarLayout;
    private UserController userController;
    private OrganizadorController organizadorController;

    private LinearLayout navHeaderMain;
    private TextView navHeaderName,navHeaderEmail;
    private PersistenUserSesion persistenUserSesion;
    TokenManager tokenManager;
    ApiService service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main_org);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        organizadorController = new OrganizadorController(this);
        //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
      //+  mFloatingSearchView =  findViewById(R.id.floating_search_view);
        mAppBarLayout =  findViewById(R.id.appbar_organizador);
        navHeaderMain = findViewById(R.id.nav_header_main);
        navHeaderName = findViewById(R.id.head_name_seion);
        navHeaderEmail = findViewById(R.id.head_email_sesion);
        organizadorController.eventosOrganizador(this);



        // Set up the ViewPager with the sections adapter.
        //mViewPager =  findViewById(R.id.container);
        //mViewPager.setAdapter(mSectionsPagerAdapter);

        //TabLayout tabLayout =  findViewById(R.id.tabs);
        tokenManager = TokenManager.getInstance(getSharedPreferences(getLocalClassName(), MODE_PRIVATE));
        persistenUserSesion = new PersistenUserSesion(OrganizadorMainActivity.this);
            //navHeaderEmail.setText("Prueba");
            //navHeaderName.setText("correoElectronico");




        FloatingActionButton fab =  findViewById(R.id.fab_insert_event);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(OrganizadorMainActivity.this, InsertActivity.class);
                intent.putExtra("id_usuario", "b");

                startActivity(intent);
                finish();


                // getActivity().startActivityForResult(new Intent(getActivity(), InsertActivity.class).putExtra("id_usuario",b), 3);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //     .setAction("Action", null).show();
            }
        });
        if (savedInstanceState == null) {

            String h = getIntent().getExtras().getString(Constantes.ID_USER);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_org, MainFragment.createInstance(h), "MainFragment")
                    .commit();
        }


        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

      //mFloatingSearchView.attachNavigationDrawerToMenuButton(drawer);


    }


    public OrganizadorController getOrganizadorController() {
        return organizadorController;
    }

    public void setOrganizadorController(OrganizadorController organizadorController) {
        this.organizadorController = organizadorController;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_eventos) {
            // Handle the camera action
        } else if (id == R.id.nav_notificaciones) {



        } else if (id == R.id.nav_pendientes) {
            Intent intent = new Intent(OrganizadorMainActivity.this, ComentariosActivity.class);
            intent.putExtra("tokenManager", tokenManager.getToken().getAccessToken());
            startActivity(intent);

        } else if (id == R.id.nav_chat) {
            Intent intent = new Intent(OrganizadorMainActivity.this, ChatActivity.class);
            intent.putExtra("id_usuario", "1");
            startActivity(intent);


        } else if (id == R.id.nav_config) {

            Intent intent = new Intent(OrganizadorMainActivity.this, PerfilActivity.class);
            intent.putExtra("id_usuario", "1");
            startActivity(intent);


        } else if (id == R.id.nav_sesion) {
            tokenManager.deleteToken();
            persistenUserSesion.deleteSesion();
            Intent intent = new Intent(OrganizadorMainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





}

