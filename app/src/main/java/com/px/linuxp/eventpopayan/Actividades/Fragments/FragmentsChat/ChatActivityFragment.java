package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsChat;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.px.linuxp.eventpopayan.Adapters.ChatAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;
import com.px.linuxp.eventpopayan.Modelo.Chat;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ChatActivityFragment extends Fragment {


    private ChatAdapter chatAdapter;
    private RecyclerView lista;
    private RecyclerView.LayoutManager lManager;
    private FloatingActionButton fab;
    String id="";
    private static final String ARG_SECTION_NUMBER = "section_number";

    public ChatActivityFragment() {
    }
    public static ChatActivityFragment newInstance(String idChat) {
        ChatActivityFragment chatFragment = new ChatActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.EXTRA_ID_CHAT, idChat);
        chatFragment.setArguments(bundle);
        return chatFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        lista = (RecyclerView) rootView.findViewById(R.id.id_chat_recicler);
        lista.setHasFixedSize(true);
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab_insert_chat);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String h1 = getActivity().getIntent().getExtras().getString(Constantes.ID_USER);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.id_container_chat, MensajesDetailChatFragment.newInstance(h1), "DetailChat")
                        .commit();

                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });


        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);
        procesarRespuesta();


        return rootView;

    }
    private void procesarRespuesta() {
        /*Chat chat = new Chat(1,"Cristian","adrres","11-04-2017","url");
        Chat chat1 = new Chat(1,"Cristian","adrres","11-04-2017","url");
        Chat chat2 = new Chat(1,"Cristian","adrres","11-04-2017","url");
        Chat chat3 = new Chat(1,"Felipe","adrres","11-04-2017","url");
        Chat chat4 = new Chat(1,"Andrea","adrres","11-04-2017","url");
        Chat chat5 = new Chat(1,"Maria","adrres","11-04-2017","url");
        Chat chat6 = new Chat(1,"Jose","adrres","11-04-2017","url");
        Chat chat7 = new Chat(1,"Marcela","adrres","11-04-2017","url");
        Chat chat8 = new Chat(1,"Viviana","adrres","11-04-2017","url");*/




        /*List<Chat> chats = new ArrayList<>();
        chats.add(chat);
        chats.add(chat1);
        chats.add(chat2);
        chats.add(chat3);
        chats.add(chat4);
        chats.add(chat5);
        chats.add(chat6);
        chats.add(chat7);
        chats.add(chat8);*/
        List<Chat> chats = new ArrayList<>();





        chatAdapter = new ChatAdapter(chats,getActivity());
        // Setear adaptador a la lista
        lista.setAdapter(chatAdapter);


    }
}
