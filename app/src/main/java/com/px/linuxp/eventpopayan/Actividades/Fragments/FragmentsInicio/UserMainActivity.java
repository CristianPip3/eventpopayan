package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.px.linuxp.eventpopayan.Actividades.GUI.ChatActivity;
import com.px.linuxp.eventpopayan.Actividades.SectionAdapters.SectionsPagerAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.R;

public class UserMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener{

    private ViewPager mViewPager;
    private Toolbar toolbar;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FloatingSearchView mFloatingSearchView;
    private AppBarLayout mAppBarLayout;
    private PersistenUserSesion persistenUserSesion;
    private UserController userController;
    private OrganizadorController organizadorController;
    TokenManager tokenManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_usr);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        //mFloatingSearchView =  findViewById(R.id.floating_search_view);
        mAppBarLayout =  findViewById(R.id.appbar_usr);

        // Set up the ViewPager with the sections adapter.
        mViewPager =  findViewById(R.id.view_container_usr);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        userController = new UserController();


        TabLayout tabLayout =  findViewById(R.id.tabs_usr);
        tokenManager = TokenManager.getInstance(getSharedPreferences(getLocalClassName(), MODE_PRIVATE));
        persistenUserSesion = new PersistenUserSesion(UserMainActivity.this);

        /*if(tokenManager.getToken().getAccessToken() != null){
            /*if (persistenUserSesion.isLoadedData()){
                startActivity(new Intent(this, InicioActivity.class));
                finish();
            }else{
                startActivity(new Intent(this, UserMainActivity.class));
                finish();
            }
        }*/
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(this);



        DrawerLayout drawer =  findViewById(R.id.drawer_layout_usr);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view_usr);
        navigationView.setNavigationItemSelectedListener(this);
        //mFloatingSearchView.attachNavigationDrawerToMenuButton(drawer);

    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constantes.CODIGO_DETALLE || requestCode == 3) {
            if (resultCode == RESULT_OK || resultCode == 203) {
                MainFragment fragment = (MainFragment) getSupportFragmentManager().
                        findFragmentByTag("MainFragment");

            }
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        userController.ObtenerEventos();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout_usr);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_eventos_a) {
            // Handle the camera action
        } else if (id == R.id.nav_chat_a) {
            Intent intent = new Intent(UserMainActivity.this, ChatActivity.class);
            intent.putExtra("id_usuario", "1");
            startActivity(intent);


        } else if (id == R.id.nav_config_a) {

            Intent intent = new Intent(UserMainActivity.this, PerfilActivity.class);
            intent.putExtra("id_usuario", "1");
            startActivity(intent);


        } else if (id == R.id.nav_sesion_a) {
            tokenManager.deleteToken();
            persistenUserSesion.deleteSesion();
            Intent intent = new Intent(UserMainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();


        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout_usr);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()){
            case 0:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.populares));
                break;
            case 1:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.recreativos));
                break;
            case 2:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.socilaes));
                break;
            case 3:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.salud));
                break;
            case 4:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.academicos));
                break;
            case 5:
                mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.religiosos));
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

}
