package com.px.linuxp.eventpopayan.Herramientas.Entites;

import com.px.linuxp.eventpopayan.Modelo.ResponseComentarioData;

public class ResponseComentario {
    private ResponseComentarioData[] data;

    public ResponseComentarioData[] getData() {
        return this.data;
    }

    public void setData(ResponseComentarioData[] data) {
        this.data = data;
    }
}
