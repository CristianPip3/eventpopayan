package com.px.linuxp.eventpopayan.Actividades.Dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

import com.px.linuxp.eventpopayan.Actividades.GUI.InicioActivity;
import com.px.linuxp.eventpopayan.Actividades.GUI.Registro;
import com.px.linuxp.eventpopayan.R;

/**
 * Created by linuxp on 10/03/18.
 */

public class SelectionCuentaDialog extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createSingleListDialog();
    }

    /**
     * Crea un Diálogo con una lista de selección simple
     *
     * @return La instancia del diálogo
     */
    public AlertDialog createSingleListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.crear_cuenta, null);
        builder.setView(mView);
        builder.show();
        Button buttonCrearCuenta = mView.findViewById(R.id.Di_cr_cu_btn_crear_cuenta);


        return builder.create();
    }

}
