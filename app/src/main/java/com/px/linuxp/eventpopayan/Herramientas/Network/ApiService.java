package com.px.linuxp.eventpopayan.Herramientas.Network;

import com.px.linuxp.eventpopayan.Herramientas.Entites.AccessToken;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Entites.ResponseComentario;
import com.px.linuxp.eventpopayan.Modelo.NewEvento;
import com.px.linuxp.eventpopayan.Modelo.ResponseInsert;
import com.px.linuxp.eventpopayan.Herramientas.Entites.UserResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by linuxp on 27/01/18.
 */

public interface ApiService {

    @POST("register")
    @FormUrlEncoded
    Call<AccessToken> register(@Field("name") String name,
                               @Field("email") String email,
                               @Field("password") String passwor);

    @POST("login")
    @FormUrlEncoded
    Call<AccessToken> login(@Field("username") String username,
                            @Field("password") String password);

    @POST("store")
    @FormUrlEncoded
    Call<ResponseInsert> store(@Field("id_guia") int id_guia,
                               @Field("name") String name,
                               @Field("Categoria") String categoria,
                               @Field("descripcion") String descripcion,
                               @Field("hora") String hora,
                               @Field("fecha_inicio") String fecha_inicio,
                               @Field("fecha_fin") String fecha_fin,
                               @Field("precio") String precio,
                               @Field("lugar_evento") String punto_encuentro,
                               @Field("responsable") String responsable);
    @POST("storeComent")
    @FormUrlEncoded
    Call<ResponseInsert> storeComentario(@Field("id_guia") int id_guia,
                               @Field("id_asistente") int id_asisitente,
                               @Field("descripcion") String descripcion,
                               @Field("puntuacion") String puntuacion);
    @Multipart
    @POST("storeI")
    @FormUrlEncoded
    Call<ResponseInsert> storeI(@Field("id_guia") int id_guia,
                                @Part MultipartBody.Part image,
                               @Field("name") String name,
                               @Field("categoria") String categoria,
                               @Field("descripcion") String descripcion,
                               @Field("hora") String hora,
                               @Field("fecha_inicio") String fecha_inicio,
                               @Field("fecha_fin") String fecha_fin,
                               @Field("precio") String precio,
                               @Field("lugar_evento") String punto_encuentro,
                               @Field("responsable") String responsable);

    @POST("updateorg")
    @FormUrlEncoded
    Call<ResponseInsert> updateOrg(@Field("id") int id,
                                   @Field("name") String name,
                                   @Field("cedula") String cedula,
                                   @Field("descripcion_perfil") String descripcion_perfil,
                                   @Field("telefono") String telefono,
                                   @Field("genero") String genero);
    @POST("updateusr")
    @FormUrlEncoded
    Call<ResponseInsert> updateUser(@Field("id") int id,
                              @Field("name") String name,
                              @Field("cedula") String cedula,
                              @Field("descripcion_perfil") String descripcion_perfil,
                              @Field("telefono") String telefono,
                              @Field("genero") String genero);


    @POST("social_auth")
    @FormUrlEncoded
    Call<AccessToken> socialAuth(@Field("name") String name,
                                 @Field("email") String email,
                                 @Field("provider") String provider,
                                 @Field("provider_user_id") String providerUserId);

    @POST("refresh")
    @FormUrlEncoded
    Call<AccessToken> refresh(@Field("refresh_token") String refreshToken);

    @GET("posts")
    Call<PostResponse> posts();

    @GET("comentarios")
    Call<ResponseComentario> comentarios();

    @GET("eventosAll")
    Call<PostResponse> eventAll();
    /*Comentarios Gastronomicos*/
    @GET("eventosGastro")
    Call<PostResponse> eventGastro();
    /*Comentarios Sociales*/
    @GET("eventosSoci")
    Call<PostResponse> eventSocial();
    @GET("eventosRecre")
    Call<PostResponse> eventRecreativo();
    @GET("eventosReli")
    Call<PostResponse> eventReligioso();
    @GET("eventosSalud")
    Call<PostResponse> eventSalud();
    @GET("eventosAca")
    Call<PostResponse> eventAcademico();





    @GET("profile")
    Call<UserResponse> profile();
}
