package com.px.linuxp.eventpopayan.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Modelo.Mensaje;
import com.px.linuxp.eventpopayan.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by linuxp on 22/11/17.
 */

public class MensajeAdapter extends RecyclerView.Adapter<MensajeAdapter.MensajeViewHolder>
        implements ItemClickListener {

    /**
     * Lista de objetos {@link Mensaje} que representan la fuente de datos
     * de inflado
     */
    private List<Mensaje> items = new ArrayList<>();

    /*
    Contexto donde actua el recycler view
     */
    private Context context;

    public MensajeAdapter( Context context){
        this.context = context;


    }
    public void addMensaje(Mensaje mensaje){
        items.add(mensaje);
        notifyItemInserted(items.size());



    }
    public void llenarMensjaR(){

        Mensaje ms = new Mensaje("1","Pipe","Hola Pipe","20:34","1");
        Mensaje ms1 = new Mensaje("1","Pipe","Hola Pipe","20:34","2");
        Mensaje ms2 = new Mensaje("1","Pipe","Hola Pipe","20:34","1");
        Mensaje ms3 = new Mensaje("1","Pipe","Hola Pipe","20:34","2");
        Mensaje ms4 = new Mensaje("1","Pipe","Hola Pipe","20:34","1");
        Mensaje ms5 = new Mensaje("1","Pipe","Hola Pipe","20:34","2");
        Mensaje ms6 = new Mensaje("1","Pipe","Hola Pipe","20:34","1");
        Mensaje ms7 = new Mensaje("1","Pipe","Hola Pipe","20:34","2");
        items.add(ms);
        items.add(ms1);
        items.add(ms2);
        //items.add(ms3);
        //items.add(ms4);
        //items.add(ms5);
        //items.add(ms6);
        //items.add(ms7);





    }
    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public MensajeViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).
                inflate(R.layout.item_list_chat_r, viewGroup, false);
        return new MensajeAdapter.MensajeViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(MensajeViewHolder viewHolder, int position) {
        RelativeLayout.LayoutParams rm = (RelativeLayout.LayoutParams) viewHolder.linearLayoutmsj.getLayoutParams();
        RelativeLayout.LayoutParams irm = (RelativeLayout.LayoutParams)viewHolder.imageView.getLayoutParams();
        if (items.get(position).getTipoMensaje().equals("1")){

            rm.removeRule(RelativeLayout.ALIGN_PARENT_START);
            rm.addRule(RelativeLayout.ALIGN_PARENT_END);

            irm.removeRule(RelativeLayout.ALIGN_PARENT_END);
            irm.addRule(RelativeLayout.ALIGN_PARENT_START);

        }else if (items.get(position).getTipoMensaje().equals("2")){
            //
            rm.removeRule(RelativeLayout.ALIGN_PARENT_END);
            rm.addRule(RelativeLayout.ALIGN_PARENT_START);

            irm.removeRule(RelativeLayout.ALIGN_PARENT_START);
            irm.addRule(RelativeLayout.ALIGN_PARENT_END);
        }
        viewHolder.linearLayoutmsj.setLayoutParams(rm);
        viewHolder.imageView.setLayoutParams(irm);

        viewHolder.msjRecibio.setText(items.get(position).getFechaMensaje());
        viewHolder.msjMsj.setText(items.get(position).getMensaje());

    }


    @Override
    public void onItemClick(View view, int position) {

    }


    public static class MensajeViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public CardView cardView;
        public LinearLayout linearLayoutmsj;
        public CircleImageView imageView;
        public TextView msjMsj;
        public TextView msjRecibio;



        public ItemClickListener listener;

        public MensajeViewHolder(View v, ItemClickListener listener) {
            super(v);
            msjMsj = (TextView) v.findViewById(R.id.id_CVC_mensaje_r);
            msjRecibio = (TextView) v.findViewById(R.id.id_CVCR_fecha_r);
            cardView = (CardView) v.findViewById(R.id.CV_chat);
            linearLayoutmsj = (LinearLayout) v.findViewById(R.id.id_lyt_mensaje);
            imageView = (CircleImageView) v.findViewById(R.id.fotoUsuario);
            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }
}
