package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.px.linuxp.eventpopayan.Herramientas.Constantes.PersistenUserSesion;
import com.px.linuxp.eventpopayan.Herramientas.Entites.UserResponse;
import com.px.linuxp.eventpopayan.Modelo.UserResponseUsuario;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 12/04/18.
 */

public class ServiceConect extends IntentService {
    private static final String TAG = ServiceConect.class.getSimpleName();
    ApiService serviceAut;
    TokenManager tokenManager;
    public static  boolean tipo = true;
    Call<UserResponse> call;
    UserResponseUsuario user;
    PersistenUserSesion persistenUserSesion;

    static final String terminoOk = "terminoOk";
    static final String terminoError = "teminoError" ;
    public ServiceConect() {
        super("ServiceConect");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        revisarPerfil();

    }
    public  boolean revisarPerfil(){
        final boolean bandera = false;
        persistenUserSesion = new PersistenUserSesion(this);
        tokenManager = TokenManager.getInstance(this.getSharedPreferences("pref",MODE_PRIVATE));
        serviceAut = RetrofitBuilder.createServiceWithAuth(ApiService.class,tokenManager);
        call = serviceAut.profile();
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    persistenUserSesion.setLoadedData(response.body().getUsuario().is_org(),
                            response.body().getUsuario().getId(),
                            response.body().getUsuario().getCedula(),
                            response.body().getUsuario().getEmail(),
                            response.body().getUsuario().getName());

                        tokenManager.setOrganizador(response.body().getUsuario().is_org());
                        notifiFin();



                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                notifiError();

            }
        });
        return bandera;
    }
    public void notifiFin(){
        Intent intent = new Intent();
        intent.setAction(terminoOk);
        sendBroadcast(intent);


    }
    public void notifiError(){
        Intent intent = new Intent();
        intent.setAction(terminoError);
        sendBroadcast(intent);


    }
}
