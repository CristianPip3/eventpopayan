package com.px.linuxp.eventpopayan.Modelo;

/**
 * Created by linuxp on 20/11/17.
 */

public abstract class Persona {
    private int id;
    private String name;
    private String email;



    public Persona() {

    }

    public Persona(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
