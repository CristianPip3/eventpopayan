package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio;

import android.content.Context;
import android.util.Log;

import com.px.linuxp.eventpopayan.Adapters.EventAdapter;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Entites.UserResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.Modelo.NewEvento;
import com.px.linuxp.eventpopayan.Modelo.NewEventoData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by linuxp on 21/03/18.
 */



public class UserController {
    private static final String TAG = "UserController";

    List<Object> obj;
    ApiService service;
    ApiService serviceAut;
    TokenManager tokenManager;
    Call<PostResponse> call;
    Call<UserResponse> call1;

    public UserController() {
    }

    public UserController(List<Object> obj, ApiService service, TokenManager tokenManager, Call<PostResponse> call, ApiService serviceAut, Call<UserResponse> call1) {
        this.obj = obj;
        this.service = service;
        this.tokenManager = tokenManager;
        this.serviceAut = serviceAut;
        this.call1 = call1;

        this.call = call;
    }
    public void ObtenerEventos(){
        //EventoSugar.deleteAll(EventoSugar.class);
        service = RetrofitBuilder.createService(ApiService.class);
        call = service.eventAll();
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                Log.w(TAG, "onResponse: Respuesta " + response);

                if(response.isSuccessful()){
                    procesarRespuesta(response);
                    Log.w(TAG, "CATEGORIA " + response.body().getData().get(1).getCategoria());

                }else {
                   // tokenManager.deleteToken();
                    //startActivity(new Intent(OrganizadorMainActivity.this, LoginActivity.class));


                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage() );
            }
        });
    }
    public void procesarRespuesta(Response<PostResponse> response) {
        for (NewEventoData evento:response.body().getData()

                ) {
            EventoSugar eventoSugar = new EventoSugar(evento);
            eventoSugar.save();
        }
    }
    public void configurarVista(){}
    public void realizarComentario(){}
    public void asistirEvento(){}
    public void cerrarSesion(){}

    public void setObj(List<Object> obj) {
        this.obj = obj;
    }

    public void setService(ApiService service) {
        this.service = service;
    }

    public void setTokenManager(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    public void setCall(Call<PostResponse> call) {
        this.call = call;
    }
}
/*serviceAut = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        call1 = serviceAut.profile();
        call1.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Log.w(TAG, "onResponse: " + response );
                System.out.println(response.body().getUsuario());


                if(response.isSuccessful()){
                    response.body().getUsuario();
                    //title.setText(response.body().getData().get(0).getTitle());
                    Log.w(TAG, "onResponse: "  );
                }else {
                    tokenManager.deleteToken();
                    //startActivity(new Intent(OrganizadorMainActivity.this, LoginActivity.class));
                    //finish();

                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.w(TAG, "onFailure: " + t.getMessage() );
            }
        });*/
