package com.px.linuxp.eventpopayan.Persistencia;

import com.px.linuxp.eventpopayan.Herramientas.Entites.ApiError;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by linuxp on 1/03/18.
 */

public class Utils {
    public static ApiError converErrors(ResponseBody response){
        Converter<ResponseBody, ApiError> converter = RetrofitBuilder.getRetrofit().responseBodyConverter(ApiError.class, new Annotation[0]);

        ApiError apiError = null;

        try {
            apiError = converter.convert(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiError;
    }
}
