package com.px.linuxp.eventpopayan.Persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.px.linuxp.eventpopayan.Herramientas.Constantes.ConstatesDB;

/**
 * Created by linuxp on 20/11/17.
 */

public class MyBaseData extends SQLiteOpenHelper {




    public MyBaseData(Context context) {
        super(context, ConstatesDB.DATABASE_NAME, null, ConstatesDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
