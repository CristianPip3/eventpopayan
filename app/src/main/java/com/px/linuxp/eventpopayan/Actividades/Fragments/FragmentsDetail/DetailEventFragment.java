package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsMain.PopularesFragment;
import com.px.linuxp.eventpopayan.Herramientas.Entites.PostResponse;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.Herramientas.Network.RetrofitBuilder;
import com.px.linuxp.eventpopayan.Modelo.Evento;
import com.px.linuxp.eventpopayan.Modelo.EventoSugar;
import com.px.linuxp.eventpopayan.R;
import com.px.linuxp.eventpopayan.Herramientas.Constantes.Constantes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailEventFragment extends Fragment {
    private static final String TAG = PopularesFragment.class.getSimpleName();
    ApiService service;
    Call<PostResponse> call;
    private Toolbar mToolbar;
    private TextView txtIname;
    private TextView txtIcategoria;
    private TextView txtIdescripcion;
    private TextView txtIpunto_encuentro;
    private TextView txtIrespondable;
    private TextView txtIfecha_inicio;
    private TextView txtIimagen_evento;
    private TextView txtIprecio;
    private TextView txtIfecha_fin;



    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Long idEvento;

    private OnFragmentInteractionListener mListener;

    public DetailEventFragment() {
        // Required empty public constructor
    }
    public static DetailEventFragment createInstance(Long idEvento) {
        DetailEventFragment detailFragment = new DetailEventFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constantes.EXTRA_ID, idEvento);
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    void procesarRespuesta(EventoSugar e){
        txtIname.setText(e.getName());
        txtIdescripcion.setText(e.getDescripcion());
        txtIcategoria.setText(e.getCategoria());
        txtIfecha_inicio.setText(e.getFecha_inicio());
        txtIfecha_fin.setText(e.getFecha_fin());
        txtIprecio.setText(e.getPrecio());
        txtIpunto_encuentro.setText(e.getPunto_encuentro());

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idEvento = getArguments().getLong(Constantes.EXTRA_ID);

        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detail_event, container, false);
        // Inflate the layout for this fragment

        txtIname =rootView.findViewById(R.id.detAsis_txt_nombre);
        txtIcategoria =rootView.findViewById(R.id.detAsis_txt_tipo);
        txtIdescripcion =rootView.findViewById(R.id.detAsis_txt_descripcion);
        txtIpunto_encuentro =rootView.findViewById(R.id.detAsis_txt_pto_encuentro);
        txtIrespondable =rootView.findViewById(R.id.detAsis_txt_nombre_org);
        txtIfecha_inicio =rootView.findViewById(R.id.detAsis_txt_fecha_inicio);
        txtIprecio =rootView.findViewById(R.id.detAsis_txt_precio);
        txtIfecha_fin =rootView.findViewById(R.id.detAsis_txt_fecha_fin);

        List<EventoSugar> listSugar = EventoSugar.listAll(EventoSugar.class);

        System.out.println(idEvento+"XXXXXXXXXXXXXXXXXXXXXXXXXXX");
        EventoSugar eventoSugar = EventoSugar.findById(EventoSugar.class,idEvento);
        for (int i = 0; i < listSugar.size(); i++) {
            System.out.println(listSugar.get(i).getId()+"AAAAAAAAAAAAAAAAA");

        }

        if(eventoSugar!=null){
            System.out.println(eventoSugar.getName()+"oooooooo");
            procesarRespuesta(eventoSugar);

        }




        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
