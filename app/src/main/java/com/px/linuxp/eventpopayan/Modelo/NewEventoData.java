package com.px.linuxp.eventpopayan.Modelo;

public class NewEventoData {
    private String descripcion;
    private String fecha_inicio;
    private String responsable;
    private String hora;
    private String created_at;
    private String lugar_evento;
    private Object imagen_evento;
    private String precio;
    private int id_guia;
    private String Categoria;
    private Object updated_at;
    private String fecha_fin;
    private String name;
    private int id;

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha_inicio() {
        return this.fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getResponsable() {
        return this.responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getHora() {
        return this.hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLugar_evento() {
        return this.lugar_evento;
    }

    public void setLugar_evento(String lugar_evento) {
        this.lugar_evento = lugar_evento;
    }

    public Object getImagen_evento() {
        return this.imagen_evento;
    }

    public void setImagen_evento(Object imagen_evento) {
        this.imagen_evento = imagen_evento;
    }

    public String getPrecio() {
        return this.precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getId_guia() {
        return this.id_guia;
    }

    public void setId_guia(int id_guia) {
        this.id_guia = id_guia;
    }

    public String getCategoria() {
        return this.Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public Object getUpdated_at() {
        return this.updated_at;
    }

    public void setUpdated_at(Object updated_at) {
        this.updated_at = updated_at;
    }

    public String getFecha_fin() {
        return this.fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
