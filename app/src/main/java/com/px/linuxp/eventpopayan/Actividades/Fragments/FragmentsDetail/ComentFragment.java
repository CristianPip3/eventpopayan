package com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsDetail;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.px.linuxp.eventpopayan.Actividades.Fragments.FragmentsInicio.TokenManager;
import com.px.linuxp.eventpopayan.Modelo.ResponseInsert;
import com.px.linuxp.eventpopayan.Herramientas.Network.ApiService;
import com.px.linuxp.eventpopayan.R;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by linuxp on 15/01/18.
 */

public class ComentFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = ComentFragment.class.getSimpleName();
    private MaterialRatingBar mRatingBar;
    private TextView mRatingScale;
    private TextInputLayout txTIdescripcion;
    private View view;
    private Button btnEnviar;
    private String puntuacion;
    ApiService service;
    Call<ResponseInsert> call;
    TokenManager tokenManager;


    public ComentFragment() {
        super();
    }


    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.publicar_comentario, container, false);
        mRatingBar = rootView.findViewById(R.id.comOrg_rtb_org);
        mRatingScale = rootView.findViewById(R.id.comOrg_txt_mensaje_escala);
        txTIdescripcion = rootView.findViewById(R.id.txtIDescripcion);
        btnEnviar = rootView.findViewById(R.id.comOrg_btn_enviar_comentario);
        view = rootView.findViewById(R.id.view_linear_coment);
        btnEnviar.setOnClickListener(this);

        addListenerOnRatingBar();
        return rootView;
    }
    void register(){
        String id_guia;
        String id_asistente ;
        String descripcion = txTIdescripcion.getEditText().getText().toString();
        String puntuacion = this.puntuacion;

        //

        txTIdescripcion.setError(null);


        //txtIfecha_fin.setError(null);

        // if(validator.validate()) {
        call = service.storeComentario(1,4, descripcion,puntuacion);
        call.enqueue(new Callback<ResponseInsert>() {
            @Override
            public void onResponse(Call<ResponseInsert> call, Response<ResponseInsert> response) {
                Log.w(TAG, "onResponse: " + response);
                if (response.isSuccessful()) {

                    showLoginExitoso(response.body().getData());



                } else {
                    // handleErrors(response.errorBody());
                    showLoginError(response.message());

                }
            }

            @Override
            public void onFailure(Call<ResponseInsert> call, Throwable t) {
                Snackbar.make(view, "ErrorConectiom", Snackbar.LENGTH_LONG).show();
                Log.w(TAG, "onFailure: " + t.getMessage());

            }
        });
        // }
    }
    private void showLoginError(String error) {
        Snackbar.make(view, error, Snackbar.LENGTH_LONG).show();
    }
    private void showLoginExitoso(String respuesta) {
        Snackbar.make(view, "Registro Exitoso" , Snackbar.LENGTH_LONG).show();
    }
    public void addListenerOnRatingBar() {

        mRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                mRatingScale.setText(String.valueOf(rating));
                switch ((int) ratingBar.getRating()) {
                    case 1:
                        mRatingScale.setText("Muy mal organizador");
                        puntuacion = mRatingScale.getText().toString();
                        break;
                    case 2:
                        mRatingScale.setText("Mal organizador");
                        puntuacion = mRatingScale.getText().toString();
                        break;
                    case 3:
                        mRatingScale.setText("Buen organizador");
                        puntuacion = mRatingScale.getText().toString();
                        break;
                    case 4:
                        mRatingScale.setText("Muy buen organizador");
                        puntuacion = mRatingScale.getText().toString();
                        break;
                    case 5:
                        mRatingScale.setText("Excelente organizador");
                        puntuacion = mRatingScale.getText().toString();
                        break;
                    default:
                        mRatingScale.setText("");
                }

            }
        });

       //if rating is changed,
        //display the current rating value in the result (textview) automatically

    }




    @Override
    public void onClick(View v) {
       register();
    }
}
